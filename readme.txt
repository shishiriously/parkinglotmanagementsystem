Step 1) Downloading MySQL, Java and Eclipse
1.	Go to the following link and download the MySQL workbench
	a.	https://www.mysql.com/downloads/
	b.	After running downloading the installer run the installer
	c.	Once the installer is complete it will ask you to set up your mysql username and password (remember these for later as you will need them to access your database)
2.	Go the following link to download the latest version of Java (at the time of the creation of this document the latest version is Java 10)
	a.	http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html
3.	Next download the latest version of Eclipse (at the time of the creation of this document the latest version is Eclipse Photon how ever eclipse Oxygen was used for the creation of this program)
	a.	http://www.eclipse.org/downloads/


Step 2) Linking your MySql data base with the Java Program
With out Eclipse 
1.	Go into the program src file and navigate down the following path
	a.	Open the shared file
	b.	Then open the file labelled stuffInTheServer
	c.	Then from here open the file labelled DataBaseFunctionExecutor.java with a text editing software
	d.	 Once here at lines 16 and 17 you will see two strings, one labelled login and the other labelled password. 
Replace the words with the quotation marks to the right of the labels with the username and password you created when installing mysql.
With Eclipse
2.	Launch eclipse by making a new workspace and follow the following steps
	a.	Once eclipse is launched go to file > Import
	b.	Next navigate your computers files and look for the project. Once found select the project to import it into eclipse as a new project
	c.	Next open the src package and follow this path through the packages src>shared>stuffInTheServer>DataBaseFunctionExecutor
	d.	Once here at lines 16 and 17 you will see two strings, one labelled login and the other labelled password. 
Replace the words with the quotation marks to the right of the labels with the username and password you created when installing mysql.
3.	No matter which way you choose to do this once the program runs it will create a new set of tables for all the tables needed for the program

Step 3) Running the Program
This program was designed to run on a web browser using java applets however 
since browser such as chrome no longer use applets you will need to take a few additional steps in order to run the program.
1.	No matter which method you use to run the program, you must start the server. 
To do that, open the folder server, then open the file FinalProjectServer471.java in your ide of choice. Then run it using that IDE. 
2.	Using google chrome go to the chrome webstore and download the extension called IE Tab
3.	Once the extension is added to chrome go to the file location of the project on your computer then open it and go to the jar file.
4.	Once here click on the file labeld page to open the program in chrome 
5.	Once opened click on the IE Tab icon on the top right corner of you chrome browser
6.	Once this is done you will see a warning message at the top of your browser, click on this then click yes on the resulting pop up. 
This should run the program. If you did this successfully and it gave you a null pointer exception, that means that you did not run the server. You must repeat step 1.

Since java applets are an old form of technology the above steps may not run the program. 
If you are adamant on trying to run it in a web browser, follow the steps in debugging.txt, which is in the jar folder. 
If you are fine with running it as a desktop application or if a computer specific problem not in debugging.txt occurred, 
follow the steps detailed below to run the program as a desk top application with eclipse. 
1.	Open eclipse and go to the workspace where the project is
a.	If you have not yet created a workspace simple create a new file with your computers file manager and create a new file in the location of your choice. 
Then when you launch eclipse it will prompt you to pick a workplace so simply select the file you just made
b.	Then in the eclipse window go to file>Import and select the project folder to import it into eclipse as a new eclipse project 
2.	Next go to src>server>FinalProjectServer471 then run FinalProjectServer471
3.	After the server is running go to src>client>PretendMain then run PretendMain
4.	Running the program will create the necessary tables if they don�t already exist in your MySQL database


We also wanted to note that the applet shaking while scrolling was actually a glitch in the code, not my computer. 
 we were unable to find any documentation on that error, and we followed a tutorial on applet perfectly, so we assumed it was my computer, since my computer is rather buggy. 
However, it turns out that the tutorial we used was heavily flawed, and that flaw caused the error. We have fixed the error for your convenience, so the applet will no longer shake.
