package client.communicationToServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;

import shared.clientAndServerCommunicationSystems.SenderToSocket;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ClientsInstructionsForReceivingData;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ErrorData;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ServerFunction;

/**
 * this is the class which the user interacts with, when they want to
 * communicate to the server. To make the server do something, they use the
 * sendStufToServer method.
 * 
 */
public class ServerCommunicator {

	/** The next ID. */
	private static int nextID = 0;
	
	/** The sender, which sends data to the server */
	private SenderToSocket<ServerFunction<?>> sender;
	
	/** The place where instructions on what to do after recieving from the server is stored. */
	private ArrayList<ClientsInstructionsForReceivingData<?>> storage;

	/**
	 * Instantiates a new server communicator.
	 *
	 * @param listOfWhatToDo the place to store instructions on what to do with the recieved data
	 * @param sender the sender
	 */
	public ServerCommunicator(ArrayList<ClientsInstructionsForReceivingData<?>> listOfWhatToDo,
			SenderToSocket<ServerFunction<?>> sender) {
		storage = listOfWhatToDo;
		this.sender = sender;
	}

	/**
	 * This is what you use to send instructions to the server.
	 * @param <T1> the type of data your consumer uses-> a superclass of the type of data recieved back from the server
	 * @param <T2> the type of data you recieve back from the server
	 * @param receiver the method which will recieve the data from the server, when it is successful
	 * @param errorMessageHandeler the method which will recive data from the server when there is an error
	 * @param function the function that the server will do. like do something with the database, or send a message, etc.
	 * @return the ID of the server transaction.
	 */
	public <T1, T2 extends T1> int sendStuffToServer(Consumer<T1> receiver, Consumer<ErrorData> errorMessageHandeler,
			ServerFunction<T2> function) {
		if (errorMessageHandeler == null) {
			errorMessageHandeler = ClientsCommunicationSystem.ERRORMESSAGEDISPLAYER;
		}
		if (receiver == null) {
			receiver = getDefaultReceiver(errorMessageHandeler);
		}
		if (function == null) {
			errorMessageHandeler.accept(new ErrorData("error, you tried to enter a null value for a server function",
					ErrorData.ErrorType.ILLEGALARGUMENT));
			return -1;
		}
		nextID++;

		ClientsInstructionsForReceivingData<T1> receiveInstructions = new ClientsInstructionsForReceivingData<T1>(
				nextID, errorMessageHandeler, receiver);
		storage.add(receiveInstructions);
		function.setID(nextID);
		try {
			sender.send(function);
		} catch (IOException e) {
			storage.remove(receiveInstructions);
			errorMessageHandeler.accept(new ErrorData(
					"error, we were unable to connect to the server. please contact customer support for more details",
					ErrorData.ErrorType.SERVERCOMMUNICATIONERROR));
			return -1;
		}

		return nextID;
	}

	/**
	 * This method constructs the Consumer<t1> of sendStuffToServer when null is inputed. It does this
	 * by using the inputed errorMessageDisplayer to display a success message
	 * @param <T> the generic type 
	 * @param errorMessageHandeler the error message handeler
	 * @return the default receiver
	 */
	private <T> Consumer<T> getDefaultReceiver(Consumer<ErrorData> errorMessageHandeler) {

		return new Consumer<T>() {
			@Override
			public void accept(T list) {
				errorMessageHandeler
						.accept(new ErrorData("completed successfully", ErrorData.ErrorType.DEFAULTSUCESSMESSAGE));
			}
		};
	}

}
