package client.communicationToServer;

import java.util.ArrayList;
import java.util.function.Consumer;

import javax.swing.JOptionPane;

import client.dataFieldStuff.TableCollection;
import shared.clientAndServerCommunicationSystems.SocketCommunicationConstructor;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ClientsInstructionsForReceivingData;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ErrorData;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.MessageFromServerToClient;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ServerFunction;

/**
 * this is a class which instantiates the entire Clients communication system
 * with the server. use the getSeverComunicator method to get the
 * serverComunicator. there are also a couple of static methods which are used
 * as the defualt Error Message Diplayers
 * 
 * @author brere
 */
public class ClientsCommunicationSystem {
	
	/** The Constant ERRORMESSAGEDISPLAYER, which is the defualt Error Displayer for the server system */
	public static final Consumer<ErrorData> ERRORMESSAGEDISPLAYER = ClientsCommunicationSystem::showMessageDialog;
	
	/** The Constant SERVERERRORDISPLAYER */
	private static final Consumer<String> SERVERERRORDISPLAYER = ClientsCommunicationSystem::showServerError;
	
	/** The receiving thread, which is the thread that recieves messages from the server */
	private Thread receivingThread;
	
	/** The server comunicator, which is the thing that the client communicats to the server using */
	private ServerCommunicator comunicator;

	/**
	 * Instantiates a new clients communication system.
	 */
	public ClientsCommunicationSystem() {
		// note, if you want to change the id from 1000, you must also change the server
		// side in server->FinalProjectServer

		SocketCommunicationConstructor<ServerFunction<?>, MessageFromServerToClient> constructor = new SocketCommunicationConstructor<ServerFunction<?>, MessageFromServerToClient>(
				"localhost", 1000, SERVERERRORDISPLAYER);

		ArrayList<ClientsInstructionsForReceivingData<?>> storage = new ArrayList<ClientsInstructionsForReceivingData<?>>();

		comunicator = new ServerCommunicator(storage, constructor.getSender());

		constructor.getSocketReceiver()
				.setConsumerOfOutput(new ReceiverOfMessagesFromTheServer(storage, SERVERERRORDISPLAYER));

		receivingThread = new Thread(constructor.getSocketReceiver());

		receivingThread.start();

		TableCollection.instantiateAllTables(comunicator);
	}

	/**
	 * Gets the ServerComunicator of the system.
	 * @return the comunicator
	 */
	public ServerCommunicator getComunicator() {
		return comunicator;
	}

	/**
	 * a method which is the default method for showing errors from the server, 
	 * it simply uses JOptionPane to show the message
	 * @param s the string which will be in the JOptionPane
	 */
	private static void showServerError(String s) {
		JOptionPane.showMessageDialog(null, s);
	}

	/**
	 * uses JOptionPane to show the errorMessage in the ErrorData
	 * @param s the ErrorData whos message will be shown.
	 */
	private static void showMessageDialog(ErrorData s) {
		JOptionPane.showMessageDialog(null, s.getMessage());
	}

}
