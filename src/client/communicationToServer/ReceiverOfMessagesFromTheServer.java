package client.communicationToServer;

import java.util.ArrayList;
import java.util.function.Consumer;

import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ClientsInstructionsForReceivingData;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.MessageFromServerToClient;
/**
 * this class recives messages from the server, and each time it does, it executes their printMessage function.
 * ie this should probably be a lambda expression or something, 
 * @author brere
 *
 */
//TODO turn this into a lambda expression for simplicity
public class ReceiverOfMessagesFromTheServer implements Consumer<MessageFromServerToClient>{
	private ArrayList<ClientsInstructionsForReceivingData<?>> storage;
	private Consumer<String> errorMessageDisplayer;
	public ReceiverOfMessagesFromTheServer(ArrayList<ClientsInstructionsForReceivingData<?>> storage,Consumer<String>errorMessageDisplayer) {
		this.storage=storage;
		this.errorMessageDisplayer=errorMessageDisplayer;
	}
	@Override
	public void accept(MessageFromServerToClient list) {
		try {
		list.printMessage(storage);
		}catch(IllegalArgumentException e) {
			errorMessageDisplayer.accept(e.getMessage());
		}
	}

}
