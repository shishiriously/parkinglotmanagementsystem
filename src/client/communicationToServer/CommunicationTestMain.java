package client.communicationToServer;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import client.dataFieldStuff.DataField;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ModifyingDataBaseServerFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DeletingDataBaseFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.InsertionDataBaseFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.ModificationDataBaseFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.SearchDataBaseFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.TableCreationDataBaseFunction;
/**
 * ok so this is a class that I built so you guys can test out everyhting for yoursleves, and fiddle around with it.  the instantiation looks very inimidating, but
 * its not as bad as you would think.  I tried to make it impossible for it to screw up, and you should never have to look through server code no matter how
 * stupid your error is. 
 *
 * @author brere
 *
 */
 
public class CommunicationTestMain {
	public static void main(String[] args) {
		ClientsCommunicationSystem system=new ClientsCommunicationSystem();
		ServerCommunicator communicator=system.getComunicator();
		
		DataFieldList list2=new DataFieldList(TableCollection.USERTABLE.table);
		
//		
//		communicator.sendStuffToServer(null, null,
//				new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list2)));
		list2.add(new DataField(TableCollection.USERTABLE.EMAIL,"222"));
		list2.add(new DataField(TableCollection.USERTABLE.ID,"270000"));
		list2.add(new DataField(TableCollection.USERTABLE.EMPLOYMENTSTATUS,"3"));
		list2.add(new DataField(TableCollection.USERTABLE.FIRSTNAME,"44444"));
		list2.add(new DataField(TableCollection.USERTABLE.LASTNAME,"55555"));
		list2.add(new DataField(TableCollection.USERTABLE.PASSWORD,"66666"));
		list2.add(new DataField(TableCollection.USERTABLE.PHONENUMBER,"777777"));
		//list2.add(new DataField(TableCollection.USERTABLE.SUPERSSN,null));
		list2.add(new DataField(TableCollection.USERTABLE.USERNAME,"99999"));
		
		
		communicator.sendStuffToServer(null, null, new ModifyingDataBaseServerFunction(new InsertionDataBaseFunction(list2)));
		
	//	communicator.sendStuffToServer(System.out::println, System.out::println, new ModifyingDataBaseServerFunction(new SearchDataBaseFunction(list2)));
//		communicator.sendStuffToServer(null, null, new ModifyingDataBaseServerFunction(new DeletingDataBaseFunction(list2)));
//
//		communicator.sendStuffToServer(null, null, new ModifyingDataBaseServerFunction(new ModificationDataBaseFunction(list2)));

		
	}
	
	public static void receiveToJOptionPane(ArrayList<DataFieldList> list) {
		JOptionPane.showMessageDialog(null, list.toString());
	}
	public static void receiveData(ArrayList<DataFieldList> list) {
		System.out.println(list);
	}
}