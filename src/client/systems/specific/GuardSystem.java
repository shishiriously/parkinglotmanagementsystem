package client.systems.specific;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldInputBoxConstructor;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;
import client.systems.ControlSystem;
import client.systems.util.controlSystemHandeler.ControlSystemHandler;
import client.systems.util.insertionSystem.InsertionSystem;
import client.systems.util.listSystem.ListSystem;

public class GuardSystem extends ControlSystem {
	ControlSystemHandler handler;

	public GuardSystem(ServerCommunicator s, String title) {
		super(s, title);
	}

	@Override
	public void open() {
		ArrayList<ControlSystem> systems = new ArrayList<ControlSystem>();
		systems.add(new InsertionSystem(super.getServ(),TableCollection.TICKETTABLE.table.getAsDataFieldList()));
		DataFieldList user = TableCollection.USERTABLE.table.getAsDataFieldList();
		System.out.println("guard "+this.getUserData());
		
		user.get(TableCollection.USERTABLE.SUPERSSN).getColumn()
				.setUserInputBoxConstructor(DataFieldInputBoxConstructor.createSetStringSupplierConstructor(
						this.getUserData().get(TableCollection.USERTABLE.ID).getData()));
		InsertionSystem insertUser = new InsertionSystem(super.getServ(), user);
		systems.add(insertUser);
		ListSystem testingList = new ListSystem(super.getServ(),
				TableCollection.TICKETTABLE.table.getAsDataFieldList());
		systems.add(testingList);

		handler = new ControlSystemHandler(super.getServ(), "Systems", systems);

		handler.open();

	}

	@Override
	public JPanel getDefaultPanel() {
		// TODO Auto-generated method stub
		return handler.getDefaultPanel();
	}

	@Override
	public void close() {
		handler = null;
	}

	public void addListener(ActionListener list, String title) {
		this.handler.addButton(list, title);
	}


	private void goooodMessage(ArrayList<DataFieldList> data) {
		JOptionPane.showMessageDialog(null, "Ticket Completed");
	}

}
