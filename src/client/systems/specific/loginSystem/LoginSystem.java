package client.systems.specific.loginSystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataField;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;
import client.systems.ControlSystem;
import client.systems.specific.AdminSystem;
import client.systems.specific.GuardSystem;
import client.systems.util.ControlSystemWithTitle;
import client.systems.util.SwappingPanel;
import client.systems.util.insertionSystem.UserInputView;
import client.systems.util.insertionSystem.MultiServerCallMethod;
import client.systems.util.insertionSystem.UserInputSystem;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.SearchDataBaseFunction;

/**
 * Uses InsertionSystem to insert username and password Is able to swap to
 * another page after login has been successful
 */
public class LoginSystem extends ControlSystem {

	private ControlSystem loginScreen;
	private DataFieldList list;
	private SwappingPanel outwardPanel;
	private ControlSystem current;

	public LoginSystem(ServerCommunicator s, String title) {
		super(s, title);
		list = new DataFieldList(TableCollection.USERTABLE.table);
		list.add(new DataField(TableCollection.USERTABLE.ID.copy(TableCollection.USERTABLE.ID.getColumnNumber()), null));
		DataField passwordField=new DataField(TableCollection.USERTABLE.PASSWORD.copy(TableCollection.USERTABLE.PASSWORD.getColumnNumber()), null);
		list.add(passwordField);
	}

	@Override
	public void open() {
		ArrayList<DataFieldList> arr=new ArrayList<DataFieldList>();
		arr.add(list);
		LoginUserInputSystem v = new LoginUserInputSystem(super.getServ(), arr);
		
		current =loginScreen =new ControlSystemWithTitle(v);
		loginScreen.open();
		
		outwardPanel = new SwappingPanel(loginScreen.getDefaultPanel());
	}

	private void swap(ControlSystem system) {
		if (current != this && current != null) {
			current.close();
		}
		system.open();
		current = system;
		outwardPanel.swap(system.getDefaultPanel());
	}

	@Override
	public JPanel getDefaultPanel() {
		// TODO Auto-generated method stub
		return outwardPanel;
	}

	@Override
	public void close() {
		loginScreen = null;
		current = null;
	}

	private ActionListener createSwappingListenerToLoginScreen() {
		if (loginScreen == null) {
			throw new NullPointerException();
		}
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				swap(loginScreen);
			}

		};
	}

	public void loginUpdate(ArrayList<DataFieldList> list) {
		if (list.isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Your username and password did not match any results.  Please try again");			
			return;
		} else {
			chooseWhichOneToOpen(list.get(0));
			
		}
	}

	private void chooseWhichOneToOpen(DataFieldList dataFieldList) {
		this.setUserData(dataFieldList);
		if(dataFieldList.get(TableCollection.USERTABLE.SUPERSSN).getData()==null){
			AdminSystem admin=new AdminSystem(super.getServ(), "admin");
			this.swap(admin);
			admin.addListener(createSwappingListenerToLoginScreen(), "sign out");
		}
		else {
		GuardSystem guard = new GuardSystem(super.getServ(), "Guard");
		this.swap(guard);
		guard.addListener(createSwappingListenerToLoginScreen(), "sign out");	
		}
	}
	private  class LoginUserInputSystem extends UserInputSystem{

		public LoginUserInputSystem(ServerCommunicator serv, ArrayList<DataFieldList> data) {
			super(serv, "log in", data);
		}
		@Override
		public void setView(UserInputView v) {
			super.setView(v);
			super.addDataFieldListConsumerButton( "Login", new MultiServerCallMethod(LoginSystem.this::loginUpdate, null, SearchDataBaseFunction::new,super.getServ()));

		}
		
		
		
	}
}
