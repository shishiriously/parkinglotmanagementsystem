package client.systems.specific;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldInputBoxConstructor;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;
import client.systems.ControlSystem;
import client.systems.util.controlSystemHandeler.ControlSystemHandler;
import client.systems.util.insertionSystem.InsertionSystem;
import client.systems.util.listSystem.RemovalListSystem;

public class AdminSystem extends ControlSystem {
	private ControlSystemHandler handler;

	public AdminSystem(ServerCommunicator s, String title) {
		super(s, title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void open() {
		ArrayList<ControlSystem> systems = new ArrayList<ControlSystem>();
		createInsertionPanels(systems);
		handler = new ControlSystemHandler(super.getServ(), "Systems", systems);
		handler.setDisplayTitles(false);
		handler.open();
	}

	private void createInsertionPanels(ArrayList<ControlSystem> systems) {
		// The following are all the insertions
		// Insert Tickets
		ArrayList<ControlSystem> ticketlist = new ArrayList<ControlSystem>();
//		TicketInsertionSystem tickets = new TicketInsertionSystem(super.getServ(), System.out::println,
//				this.getUserData());
//		ticketlist.add(tickets);

		RemovalListSystem removeTickets = new RemovalListSystem(super.getServ(),
				TableCollection.TICKETTABLE.table.getAsDataFieldList());
		ticketlist.add(removeTickets);
		ControlSystemHandler ticketHand = new ControlSystemHandler(super.getServ(), "Tickets", ticketlist);
		systems.add(ticketHand);

		// Insert Guards

		ArrayList<ControlSystem> guardList = new ArrayList<ControlSystem>();

		ArrayList<DataFieldList> insertGuard = new ArrayList<DataFieldList>();

		insertGuard.add(TableCollection.USERTABLE.table.getAsDataFieldList());
		insertGuard.add(TableCollection.WORKDAYS.table.getAsDataFieldList());

		// GuardInsertionSystem guards = new GuardInsertionSystem(super.getServ(),
		// TableCollection.USERTABLE.table.getAsDataFieldList(), userData,
		// TableCollection.WORKDAYS.table.getAsDataFieldList());
		// guardList.add(guards);
		InsertionSystem guardsInsert = new InsertionSystem(super.getServ(), insertGuard);
		guardList.add(guardsInsert);
		RemovalListSystem removeGuards = new RemovalListSystem(super.getServ(),
				TableCollection.USERTABLE.table.getAsDataFieldList());
		guardList.add(removeGuards);

		RemovalListSystem removeWordays=new RemovalListSystem(super.getServ(),TableCollection.WORKDAYS.table.getAsDataFieldList());
		guardList.add(removeWordays);
		
		ControlSystemHandler guardHand = new ControlSystemHandler(super.getServ(), "Guards", guardList);
		systems.add(guardHand);

		// Insert Dispute
		ArrayList<ControlSystem> disputeList = new ArrayList<ControlSystem>();
		DataFieldList disputefieldList = TableCollection.DISPUTETABLE.table.getAsDataFieldList();
		
		disputefieldList.get(TableCollection.DISPUTETABLE.CREATOR).getColumn()
				.setUserInputBoxConstructor(DataFieldInputBoxConstructor.createSetStringSupplierConstructor(
						this.getUserData().get(TableCollection.USERTABLE.ID).getData()));
		
		InsertionSystem disputes = new InsertionSystem(super.getServ(), disputefieldList);
		disputeList.add(disputes);
		RemovalListSystem removeDisputes = new RemovalListSystem(super.getServ(),
				TableCollection.DISPUTETABLE.table.getAsDataFieldList());
		disputeList.add(removeDisputes);
		ControlSystemHandler disputeHand = new ControlSystemHandler(super.getServ(), "Disputes", disputeList);
		systems.add(disputeHand);

		// Insert parking lot
		ArrayList<ControlSystem> parkingList = new ArrayList<ControlSystem>();
		InsertionSystem parkinglots = new InsertionSystem(super.getServ(),
				TableCollection.PARKINGLOTTABLE.table.getAsDataFieldList());
		parkingList.add(parkinglots);
		RemovalListSystem removeParkingLot = new RemovalListSystem(super.getServ(),
				TableCollection.PARKINGLOTTABLE.table.getAsDataFieldList());
		parkingList.add(removeParkingLot);
		ControlSystemHandler parkingHand = new ControlSystemHandler(super.getServ(), "Parking Lots", parkingList);
		systems.add(parkingHand);

		// Lot assignments
		ArrayList<ControlSystem> assignList = new ArrayList<ControlSystem>();
		InsertionSystem assignments = new InsertionSystem(super.getServ(),
				TableCollection.ASSIGNMENTS.table.getAsDataFieldList());
		assignList.add(assignments);
		RemovalListSystem removeAssignments = new RemovalListSystem(super.getServ(),
				TableCollection.ASSIGNMENTS.table.getAsDataFieldList());
		assignList.add(removeAssignments);
		ControlSystemHandler assignHand = new ControlSystemHandler(super.getServ(), "Guard Assignment", assignList);
		systems.add(assignHand);

		// Insert Vehicle
		ArrayList<ControlSystem> vehicelList = new ArrayList<ControlSystem>();
		InsertionSystem vehicles = new InsertionSystem(super.getServ(),
				TableCollection.VEHICLETABLE.table.getAsDataFieldList());
		vehicelList.add(vehicles);
		RemovalListSystem removeVehicle = new RemovalListSystem(super.getServ(),
				TableCollection.VEHICLETABLE.table.getAsDataFieldList());
		vehicelList.add(removeVehicle);
		ControlSystemHandler vehicleHand = new ControlSystemHandler(super.getServ(), "Vehicles", vehicelList);
		systems.add(vehicleHand);

		// Insert Owner
		ArrayList<ControlSystem> ownerList = new ArrayList<ControlSystem>();
		InsertionSystem owners = new InsertionSystem(super.getServ(),
				TableCollection.OWNERTABLE.table.getAsDataFieldList());
		ownerList.add(owners);
		RemovalListSystem removeOwner = new RemovalListSystem(super.getServ(),
				TableCollection.OWNERTABLE.table.getAsDataFieldList());
		ownerList.add(removeOwner);
		ControlSystemHandler ownerHand = new ControlSystemHandler(super.getServ(), "Owners", ownerList);
		systems.add(ownerHand);
		// Insert Parking pass
		ArrayList<ControlSystem> passList = new ArrayList<ControlSystem>();
		InsertionSystem parkingPasses = new InsertionSystem(super.getServ(),
				TableCollection.PARKINGPASSTABLE.table.getAsDataFieldList());
		passList.add(parkingPasses);
		RemovalListSystem removeParkingPass = new RemovalListSystem(super.getServ(),
				TableCollection.PARKINGPASSTABLE.table.getAsDataFieldList());
		passList.add(removeParkingPass);
		ControlSystemHandler passHand = new ControlSystemHandler(super.getServ(), "Parking Passes", passList);
		systems.add(passHand);
	}

	@Override
	public JPanel getDefaultPanel() {
		// TODO Auto-generated method stub
		return handler.getDefaultPanel();
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		handler = null;

	}

	public void addListener(ActionListener list, String title) {
		this.handler.addButton(list, title);
	}

}
