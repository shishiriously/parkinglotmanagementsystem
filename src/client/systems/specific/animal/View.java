package client.systems.specific.animal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 *this is a view, which represents a thing that is being shown to a user. ie its bascially a panel in the gui.
 */
public class View extends JPanel {
	
	private static final long serialVersionUID = 1L;
	JList<String> list;
	JButton but;
	public View(String name) {

		but=new JButton(name);
		list=new JList<String>();
		setSize(600,600);
		setBackground(new Color(100,200,0));
		setLayout(new BorderLayout());
		add(list);
		add(but,BorderLayout.SOUTH);
		setVisible(false);
	}
	protected void setList(DefaultListModel<String> m) {
		list.setModel(m);
	}
	public void addListener(ActionListener act) {
		but.addActionListener(act);
	}

}
