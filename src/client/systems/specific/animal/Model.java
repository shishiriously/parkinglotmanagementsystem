package client.systems.specific.animal;


import java.util.function.Consumer;

import javax.swing.*;

import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;

/**
 * this changes.  the main diffrence is that in usesData, it now use the new list.
 *
 * same model as before. simply is a defualt list model.  the only diffrence is it now implements the sets strings tag
 * and has that extra
 */
public class Model implements Consumer<DataFieldList> {

	private DefaultListModel<String> mod;

	public Model() {
		mod=new DefaultListModel<String>();	
	}

	public DefaultListModel<String>getMod(){
		return mod;
	}
	
	public void accept(DataFieldList list) {
		for(int i=0;i<list.size();i++){
			mod.addElement(list.get(TableCollection.USERTABLE.ID).getData());
		}
	}
}
