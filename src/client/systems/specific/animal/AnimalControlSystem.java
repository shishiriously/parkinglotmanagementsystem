package client.systems.specific.animal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;
import client.systems.ControlSystem;
import java.util.function.*;
/**
 * step 7.  this is basically a new control for 2.0.  it does exaclty what the old control did, only it use the new tools
 * instead of the old ones. Im not sure if we want to use this, or to use a general system like OneButtonControlSystem
 */
public class AnimalControlSystem extends ControlSystem{
    private View v;
    private Model m;
    private Supplier<DataFieldList> gets;
    public AnimalControlSystem(ServerCommunicator serv, String title, Supplier<DataFieldList> gets) {
        super(serv,title);

        this.gets=gets;
    }
    /**
     * open is when the system is activated; so when the veiw and the button appears on sceen.  In this case, when
     * veiw is activated, the jlist and the button appears on screen.
     */
    @Override
    public void open() {
        v=new View(getTitle());
        m=new Model();
        v.setList(m.getMod());

        v.addListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
					m.accept(gets.get());
			}
        	
        });
        v.setVisible(true);
    }

    @Override
    public JPanel getDefaultPanel() {
        return v;
    }
    
    /**
     * this activates whenever the system is closed, so when the veiw of the system is gon, and another
     * system is activated-ie its view is onscreen, then it activates this method.
     */
    @Override
    public void close() {
        v=null;
        m=null;
    }
}
