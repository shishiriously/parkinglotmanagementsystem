package client.systems.util.insertionSystem;



import java.util.ArrayList;
import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.ModificationDataBaseFunction;


//public class ModificationSystem extends InsertionSystem{
//
//	public ModificationSystem(ServerCommunicator s, DataFieldList list) {
//		super(s, list);
//	}
//	@Override
//	public void open() {
//		super.open();
//		super.setButtonName("modify");
//		super.getView().setAllKeysUneditable();
//		super.getView().setAllBoxesText();
//	}
//	@Override
//	protected void createServerFunction(DataFieldList list) {
//		super.getServ().sendStuffToServer(null, null, new ModifyingDataBaseServerFunction(new ModificationDataBaseFunction(list)));
//		super.getView().setAllBoxesText();
//	}
//	
//
//}


/**
 * The Class ModificationSystem, which is a UserInputSystem for modifing things in the database
 * the user can easily modify any data in the database using this system
 */
public class ModificationSystem extends UserInputSystem{
    

	 /**
		 * Instantiates a new modification system.
		 * @param s the serverCommunicator used for comunication
		 * @param list the lists that will be modified in the database
		 */
    public ModificationSystem(ServerCommunicator s, ArrayList<DataFieldList> list) {
        super(s, "modify a "+list.get(0).getTable().getTitle(), list);
    }
    
    
   /**
	 * Instantiates a new modification system.
	 * @param s the serverCommunicator used for comunication
	 * @param list the list that will be modified in the database
	 */
    public ModificationSystem(ServerCommunicator s,DataFieldList list) {
        super(s,"modify a "+list.getTable().getTitle(), new ArrayList<DataFieldList>());
    	super.getList().add(list);
    }
    
    /* (non-Javadoc)
     * @see client.systems.util.insertionSystem.UserInputSystem#setView(client.systems.util.insertionSystem.InsertionView)
     */
    @Override
    public void setView(UserInputView view)
    {
    	super.setView(view);
    	view.setAllKeysUneditable();
    	view.setAllBoxesText();
    	if(super.getUserData().get(TableCollection.USERTABLE.SUPERSSN).getData()==null) {
    		super.addDataFieldListConsumerButton("modify",new MultiServerCallMethod(null,null,this::createModifyServerFunction,super.getServ()));
    	}
    }
	
	/**
	 * Creates the modify server function.
	 *
	 * @param list the list that it will create the databaseFunction using
	 * @return the data base function for modifying
	 */
	protected DataBaseFunction createModifyServerFunction(DataFieldList list) {
		super.getView().setAllBoxesText();
		return new ModificationDataBaseFunction(list);
	}
	   
}
