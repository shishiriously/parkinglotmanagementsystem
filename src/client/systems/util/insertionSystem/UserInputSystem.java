package client.systems.util.insertionSystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataField;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.Reference;
import client.systems.ControlSystem;
import client.systems.util.ControlSystemWithTitle;
import client.systems.util.insertionSystem.individualColumnStuff.DataFieldInputManager;
import client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;
import client.systems.util.jFrameStuff.SystemInFrameHandeler;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ModifyingDataBaseServerFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.SearchDataBaseFunction;

/**
 * The Class UserInputSystem, which a system that the user can use to input things into the database, 
 * which have to do with some dataFields. like Inserting things into the database, or modifiying things 
 * in the database
 */
public class UserInputSystem extends ControlSystem {
	
	/** The list of all of the DataInputManagers, which are the things that
	 * read the users inputs, and puts them in the correct DataFields */
	private ArrayList<DataFieldInputManager> managerList;
	
	/** The data that will be inserted/modified in the database. */
	private ArrayList<DataFieldList> data;
	
	/** The v view that the user can see */
	private UserInputView v;
	
	/** The other controlSystems which the UserInputSystem starts */
	private SystemInFrameHandeler branchingSystems;

	/**
	 * Instantiates a new user input system.
	 *
	 * @param serv the servercomunicator that is used to comunicate with the server 
	 * @param title the title the title of the ControlSystem
	 * @param data the data that the System is inserting/modifying
	 */
	public UserInputSystem(ServerCommunicator serv, String title, ArrayList<DataFieldList> data) {
		super(serv, title);
		this.data = data;
		branchingSystems = new SystemInFrameHandeler();
	}

	/**
	 * Instantiates a new user input system.
	 *
	 * @param serv the servercomunicator that is used to comunicate with the server 
	 * @param title the title the title of the ControlSystem
	 * @param data the data that the System is inserting/modifying
	 */
	public UserInputSystem(ServerCommunicator serv, String title, DataFieldList data) {
		super(serv, title);
		this.data = new ArrayList<DataFieldList>();
		this.data.add(data);
		branchingSystems = new SystemInFrameHandeler();
	}

	/* (non-Javadoc)
	 * @see client.systems.ControlSystem#open()
	 */
	@Override
	public void open() {
		managerList = new ArrayList<DataFieldInputManager>();
		ArrayList<Supplier<String>> suppliers = new ArrayList<Supplier<String>>();
		for (DataFieldList list : data) {
			for (DataField field : list) {
				Supplier<String> supplier = field.getColumn().getUserInputBoxConstructor()
						.constructDataFieldInputBox(field);
				boolean requiredNewManager = addDataFieldToOtherDataFieldInputManagerIfNessicary(field, supplier, list);
				if (requiredNewManager) {
					suppliers.add(supplier);
				}
			}
		}
		this.setView(createView(suppliers));
		this.getView().setAllBoxesText();
	}

	/**
	 * Creates the view, which is the part visable to the user
	 *
	 * @param suppliers the suppliers which supply inputs to the dataFields
	 * @return the user input view that the user can see
	 */
	protected UserInputView createView(ArrayList<Supplier<String>> suppliers) {
		ArrayList<DataInputBox> box = new ArrayList<DataInputBox>();
		for (Supplier<String> sup : suppliers) {
			if (sup instanceof DataInputBox) {
				DataInputBox inputBox = (DataInputBox) sup;
				box.add(inputBox);
			}
		}
		return new UserInputView(box);
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	protected UserInputView getView() {
		return v;
	}

	/**
	 * Gets the list of each DataField that the user fills out
	 *
	 * @return the list of each DataField that the user fills out
	 */
	protected ArrayList<DataFieldList> getList() {
		return data;
	}

	/**
	 * Sets the view that the user can see
	 * @param view the new view
	 */
	protected void setView(UserInputView view) {
		v = view;
		v.setVisible(true);
	}

	/**
	 * this method puts the datafield in an already existing DataFieldInputManager
	 * if it is nessicary..
	 *
	 * @param current            the datafield which will be placed in the current input manager
	 * @param supplier the supplier
	 * @param list the list
	 * @return if the datafield required a new manager.
	 */
	private boolean addDataFieldToOtherDataFieldInputManagerIfNessicary(DataField current, Supplier<String> supplier,
			DataFieldList list) {
		if (current.getColumn() instanceof Reference && ((Reference) current.getColumn()).isAutoComplete()) {
			Reference currentRefrence = (Reference) current.getColumn();

			DataFieldInputManager resultOfSearch = getManagerRefrenceBelongsTo(currentRefrence);
			if (resultOfSearch != null) {
				resultOfSearch.add(current);
				return false;
			}
		}
		DataFieldInputManager newManager = new DataFieldInputManager(supplier, current);
		addStufftoDataInputBoxes(list, newManager);
		managerList.add(newManager);
		return true;
	}

	/**
	 * This method will add "view " and "add" buttons to the DataFieldInputManagers DataInputBox
	 * whenever (A) their supplier is a DataInputBox, and (B) when the DataInputBoxes Data is
	 * a Reference
	 *
	 * @param list the list that the DataFieldInputManagers DataFeild belongs to
	 * @param newManager the Manager that has not had the view and add buttons potentially added to it
	 */
	private void addStufftoDataInputBoxes(DataFieldList list, DataFieldInputManager newManager) {
		if (newManager.getInput() instanceof DataInputBox) {
			DataInputBox input = (DataInputBox) newManager.getInput();
			if (input.getDataField().getColumn() instanceof Reference) {
				JButton view = createViewButton(list, newManager);
				// make this better later
				input.add(view);
				JButton add = createAddButton(list, newManager);
				input.add(add);
				input.updateUI();
			}
		}

	}

	/**
	 * Creates the add button which goes next to any DataInputBox that has a Reference
	 * @param list the list that the managers dataField belongs to
	 * @param newManager the manager whichs DataInputBox will get the Add button added to it
	 * @return the j button which is displayed next to any Reference DataInputBox
	 */
	private JButton createAddButton(DataFieldList list, DataFieldInputManager newManager) {
		ActionListener listener = (ActionEvent e) -> {
			try {
				newManager.copyDataIntoDataFields();
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
				return;
			}
			DataFieldList refrenced = list
					.getPrimaryKeyOfRefrencedTable((Reference) newManager.getFirstDataField().getColumn());
			DataFieldList fullList = refrenced.getTable().getAsDataFieldList();
			refrenced.copyDataIntoAnotherDataFieldList(fullList);
			branchingSystems.addSystem(new ControlSystemWithTitle(new InsertionSystem(super.getServ(), fullList)));
		};
		JButton view = new JButton("+");
		view.addActionListener(listener);
		return view;
	}

	/**
	 * Creates the view button which goes next to any DataInputBox that has a Reference. 
	 * what the view button does is it searches for a DataFieldList that the Box refers to, and
	 * if it finds it, the onSearchSuccess method is called
	 * @param list the list that the managers dataField belongs to
	 * @param newManager the manager whichs DataInputBox will get the view button added to it
	 * @return the j button which is displayed next to any Reference DataInputBox
	 */
	// TODO MAKE THIS SUPPORT MULTI KEYS.
	private JButton createViewButton(DataFieldList list, DataFieldInputManager newManager) {
		ActionListener listener = (ActionEvent e) -> {
			try {
				newManager.copyDataIntoDataFields();
				managerList.forEach(DataFieldInputManager::temporarilyCopyDataIntoDataFields);
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
				return;
			}
			DataFieldList refrencesFromList = list
					.getPrimaryKeyOfRefrencedTable((Reference) (newManager.getFirstDataField().getColumn()));
			super.getServ().sendStuffToServer(this::onSearchSuccess, null,
					new ModifyingDataBaseServerFunction(new SearchDataBaseFunction(refrencesFromList)));
		};
		JButton view = new JButton("View");
		view.addActionListener(listener);
		return view;
	}

	/**
	 * when the search is successful, this method will launch a modificationSystem in a new JFrame
	 * @param result the result of the successful search
	 */
	private void onSearchSuccess(ArrayList<DataFieldList> result) {
		if (result.isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"error, we could not find what you searched for, please change your inputted value and try again");
			return;
		} else {
			if (result.size() > 1) {
				JOptionPane.showMessageDialog(null,
						"error, in userInputSystem, a bug made the result returned more then 1 value, therefore the keys are not unique. please contact customer support for more information");
				return;
			}
			result.set(0, result.get(0).getListWithoutHiddenItems());
			branchingSystems.addSystem(new ControlSystemWithTitle(new ModificationSystem(super.getServ(), result)));
		}
	}

	/**
	 * searches the pre-existing dataInputManagers to see if the refrence belongs to
	 * any of them.
	 *
	 * @param refrence            the refrecne which will be searched if it belongs to any pre
	 *            existing DataFieldInputManager
	 * @return the dataFieldInputManager it belongs to, or null if it does not
	 *         belong to any pre existing ones.
	 */
	private DataFieldInputManager getManagerRefrenceBelongsTo(Reference refrence) {

		for (DataFieldInputManager currentManager : managerList) {
			if (refrence.refersTo(currentManager.getFirstDataField().getColumn())) {

				return currentManager;
			}

		}
		return null;
	}

	/**
	 * adds a button to the insertion view which accepts the dataFieldLists that the user inputed
	 *
	 * @param buttonTitle the text that will be displayed on the button
	 * @param consumer what the button will do when it is activated
	 */
	public void addDataFieldListConsumerButton(String buttonTitle, Consumer<ArrayList<DataFieldList>> consumer) {
		ActionListener listener = (ActionEvent arg0) -> {
			try {
				fillList();
				consumer.accept(data);
				v.setAllBoxesText();
			} catch (IllegalArgumentException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		};
		v.addButton(buttonTitle, listener);
	}

	/**
	 * Fills the DataFieldlist with the entered data. requires this object to be
	 * opened first.
	 */
	public void fillList() {
		for (DataFieldInputManager manager : managerList) {
			manager.copyDataIntoDataFields();
		}
	}

	/**
	 * reuires this classs to be opened first.
	 *
	 * @return the default panel
	 */
	@Override
	public JPanel getDefaultPanel() {
		return v;
	}

	/* (non-Javadoc)
	 * @see client.systems.ControlSystem#close()
	 */
	@Override
	public void close() {
		resetList();
		managerList = null;
		v = null;
		branchingSystems.close();
	}

	/**
	 * Resets the list, so that no previous user inputs are visable.
	 */
	public void resetList() {
		for (DataFieldList list : data) {
			for (DataField field : list) {
				field.setData(null);
			}
		}
	}
}
