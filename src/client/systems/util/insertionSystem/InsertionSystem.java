package client.systems.util.insertionSystem;

import java.util.ArrayList;
import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.InsertionDataBaseFunction;

/**
 * this is the class that the user would use to insert stuff into the database
 * 
 * REQUIRES: the DataFieldList in the insertion view to be the same
 * datafieldlist in InsertionSystem.
 */
public class InsertionSystem extends UserInputSystem {

	/**
	 * Instantiates a new insertion system.
	 * @param s the serverCommunicator used for comunication
	 * @param list the lists that will be inserted into the database
	 */
	public InsertionSystem(ServerCommunicator s, ArrayList<DataFieldList> list) {
		super(s, "add " + list.get(0).getTable().getTitle(), list);
	}

	/**
	 * Instantiates a new insertion system.
	 * @param s the serverCommunicator used for comunication
	 * @param list the list that will be inserted into the database
	 */
	public InsertionSystem(ServerCommunicator s, DataFieldList list) {
		super(s,"add "+list.getTable().getTitle(),new ArrayList<DataFieldList>());
		super.getList().add(list);
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.UserInputSystem#setView(client.systems.util.insertionSystem.InsertionView)
	 */
	@Override
	public void setView(UserInputView view) {
		super.setView(view);
		addDataFieldListConsumerButton("add",
				new MultiServerCallMethod(null, null, this::createServerFunction, super.getServ()));
	}
	
	/**
	 * creates a database function that  the InsertionSystem wantes.
	 *
	 * @param list the list that will be ued to create the database function
	 * @return an insertion database function
	 */
	public DataBaseFunction createServerFunction(DataFieldList list) {
		return new InsertionDataBaseFunction(list);
	}

}
