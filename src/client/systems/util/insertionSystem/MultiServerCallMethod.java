package client.systems.util.insertionSystem;

import java.util.ArrayList;
import java.util.function.Consumer;

import client.communicationToServer.ClientsCommunicationSystem;
import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ErrorData;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ModifyingDataBaseServerFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction;

//this class is a mess. dont look down please
/**
 * this is a class for calling multiple server functions for multiple peices of
 * data at the same time. this can be used to simply insert or modify several
 * peices of data at the same time in a reletivly elegant way. Like if you want
 * to insert 4 peices of data at the same time, you can use a MultiServerCallMethod
 * to do that in one line.
 *
 * If there is an error while doing any given serverFunction, the method will stop the rest
 * from happening
 * @author brere
 */
public class MultiServerCallMethod implements Consumer<ArrayList<DataFieldList>> {
	
	/** the method which will construct a dataBaseFunction that will be sent to the server*/
	private DataBaseFunctionConstructor function;
	/**the serverComunicator used for comunication */
	private ServerCommunicator comunicator;
	/** the thing that will be done when a server error occurs is completed successfully */
	private Consumer<ArrayList<DataFieldList>> onSuccess;
	/** the thing that happens when there is an error*/
	private Consumer<ErrorData> onError;
	/**the name of the dataBaseFunction */
	private String databaseFunctionName;
	/**the text of the last Table sent */
	private String lastListTableTitle;
	/** the progress through the loop */
	private int i;
	/** the inputed DataFieldList */
	private ArrayList<DataFieldList> input;
	/**
	 * constructs a dataBaseFunction, using the inputed DataFieldList
	 */
	public static interface DataBaseFunctionConstructor {
		public DataBaseFunction construct(DataFieldList d);
	}
	/**
	 * constructs a multiServerCallMethod object
	 * @param onsuccess what to do when it is successful
	 * @param onerror what to do when there is an error
	 * @param function what constructs the DataBaseFunction
	 * @param comunicatorConstructor what constructs the serverComunicator
	 */
	public MultiServerCallMethod(Consumer<ArrayList<DataFieldList>> onsuccess, Consumer<ErrorData> onerror,
			DataBaseFunctionConstructor function, ServerCommunicator comunicator) {
		super();
		this.onSuccess = onsuccess;
		this.onError = onerror;
		if (onError == null) {
			onError = ClientsCommunicationSystem.ERRORMESSAGEDISPLAYER;
		}
		if (onSuccess == null) {
			onSuccess = (ArrayList<DataFieldList> d) -> {
				this.onError.accept(
						new ErrorData("your " + databaseFunctionName + " of a " + lastListTableTitle + " completed successfully",ErrorData.ErrorType.DEFAULTSUCESSMESSAGE));
			};
		}
		this.function = function;
		this.comunicator = comunicator;
	}

	

	@Override
	public void accept(ArrayList<DataFieldList> list) {
		input=list;
		i=0;
		if (!list.isEmpty()) {
			databaseFunctionName = function.construct(list.get(0)).getNameOfFunction();
		}
		sendAList(list);
	}
	/**
	 * sends a single list to the database, in a databaseFunction constructed by the databaseFunctionConstructor
	 * @param list the lsit that will be sent to the database in a databasefunction.
	 */
	private void sendAList(ArrayList<DataFieldList> list) {
		if(i>=list.size()) { return;}
		lastListTableTitle = list.get(i).getTable().getTitle();

		comunicator.sendStuffToServer(this::onSuccess, this.onError,
				new ModifyingDataBaseServerFunction(function.construct(list.get(i))));
		i++;
	}
	/**
	 * activates whenever there is a success
	 * @param d the result of a successful server call
	 */
	public void onSuccess(ArrayList<DataFieldList> d) {
		this.onSuccess.accept(d);
		sendAList(input);
	}
}
