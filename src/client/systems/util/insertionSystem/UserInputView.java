//done

package client.systems.util.insertionSystem;

import javax.swing.*;

import client.systems.util.controlSystemHandeler.MenuBar;
import client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * The Class UserInputView, which is what the UserInputSystem uses to make everything visable
 */
public class UserInputView extends JPanel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 639115811322313809L;

	/**
	 * This is an array list of all the different panels in the view. Each panel has
	 * a Label stating what should be entered into a Textbox where the data will be
	 * entered.
	 */
	private ArrayList<DataInputBox> panels;
	
	/** The main panel, which the user sees. */
	private JPanel mainPanel;
	
	/** The add Button that will add all entered data fields to the database. */
	private MenuBar bar;

	/**
	 * Basic constructor.
	 *
	 * @param panels the panels which the user can see
	 */
	public UserInputView(ArrayList<DataInputBox> panels) {
		
		this.setLayout(new BorderLayout());
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		this.bar = new MenuBar();
		this.panels = panels;
		JScrollPane pane = new JScrollPane(mainPanel);
		createUserInputBoxes(panels);
		mainPanel.updateUI();
		pane.updateUI();

		this.add(pane, BorderLayout.CENTER);

		this.updateUI();
	}

	/**
	 * This creates multiple panels depending on how many datafields are needed for
	 * the insertion operation. i.e when inserting a ticket you need vehicle make,
	 * model and license plate as well as the lot No., date, reason, ticket No. and
	 * description so it will need 8 boxes.
	 * 
	 * @param data
	 *            where the labels are coming from.
	 */
	private void createUserInputBoxes(ArrayList<DataInputBox> data) {
		GridBagConstraints cons = new GridBagConstraints();
		cons.weightx = 1;
		cons.anchor = GridBagConstraints.CENTER;

		cons.fill = GridBagConstraints.BOTH;
		cons.gridx = 0;
		cons.gridy = 0;
		cons.ipady = 60;

		for (DataInputBox input : data) {
					cons.weighty = .5;
					mainPanel.add(input, cons);
					cons.gridy++;
		}
		cons.weighty = .3;
		bar.setBackground(new Color(0, 0, 0));

		mainPanel.add(bar, cons);
	}

	

	/**
	 * Adds a button, so that the user can interact with it
	 * @param buttonTitle the title that the button will display
	 * @param listener the actionListener that the button have
	 */
	public void addButton(String buttonTitle, ActionListener listener) {
		bar.addButton(listener, buttonTitle);
		this.updateUI();
	}

	

	

	/**
	 * make all of the text that are in the DataInputBoxs invisable
	 */
	public void setTextInvisable() {
		for (DataInputBox box : panels) {
			box.setTextInvisable();
		}
	}

	/**
	 * Gets the DataInputBoxes that the user interacts with
	 * @return the panels that the user interacts with
	 */
	public ArrayList<DataInputBox> getPanels() {
		return panels;
	}

	/**
	 * Sets the all keys that are in the UserInputView uneditable
	 */
	protected void setAllKeysUneditable() {
		for (int i = 0; i < panels.size(); i++) {
			if (panels.get(i).getDataField().getColumn().isKey()) {
				panels.get(i).setEditable(false);
			}
		}
	}

	/**
	 * Sets the text of each box so that the data saved in the Datafields are now visable to the user
	 * through the dataInputBoxes
	 */
	protected void setAllBoxesText() {
		for (int i = 0; i < panels.size(); i++) {
			panels.get(i).setTextVisable();
		}
	}
}