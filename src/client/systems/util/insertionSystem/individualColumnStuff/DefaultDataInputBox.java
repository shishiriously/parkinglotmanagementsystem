package client.systems.util.insertionSystem.individualColumnStuff;

import java.awt.Font;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import client.dataFieldStuff.DataFieldInputBoxConstructor;
import client.dataFieldStuff.DataField;

/**
 * The Class DefaultDataInputBox, which is the default DataInputBox that each
 * Column uses. it uses a simple JTextField for the user to input any input they
 * want.
 */
public class DefaultDataInputBox extends DataInputBox {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The field the user inputs on. */
	private JTextField field;

	/**  The label which tells the user what to input. */
	private JLabel label;

	/**
	 * returns a DataFieldInputBoxConstructor which constructs a DefualtDataInputBox.
	 *
	 * @return a DataFieldInputBoxConstructor which constructs a DefualtDataInputBox
	 */
	public static DataFieldInputBoxConstructor constuctDataFieldInputBox() {
		return (DataField s) -> {
			return new DefaultDataInputBox(s);
		};
	}

	/**
	 * Instantiates a new default data input box.
	 *
	 * @param data That the DataInputBox holds
	 */
	public DefaultDataInputBox(DataField data) {
		super(data, .1);

		this.label = new JLabel(data.getColumn().getColumnName());
		this.label.setHorizontalAlignment(SwingConstants.TRAILING);
		this.label.setFont(new Font("Times New Roman", Font.PLAIN, 25));

		this.field = new JTextField();
		this.field.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		this.field.setColumns(30);
		this.field.setDocument(new JTextFieldLimit(super.getDataField().getColumn().getMaxSize()));
		this.setEditable(true);

		this.add(this.label);
		this.add(this.field);
		this.setVisible(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#
	 * setEditable(boolean)
	 */
	public void setEditable(boolean edit) {
		this.field.setEditable(edit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#
	 * setUserInputedText(java.lang.String)
	 */
	public void setUserInputedText(String s) {
		field.setText(s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#
	 * setTextInvisable()
	 */
	public void setTextInvisable() {
		field.setText("");
	}

	/**
	 * Gets the Data that the DataInputBox holds
	 * @return the Data that the DataInputBox holds
	 */
	public JTextField getField() {
		return field;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.function.Supplier#get()
	 */
	@Override
	public String get() {
		return field.getText();
	}

	/**
	 * Sets the field that the user will input stuff using
	 * @param field the JtextField the user will input stuff using
	 */
	public void setField(JTextField field) {
		field.setText(this.field.getText());
		this.field.setVisible(false);
		this.remove(this.field);

		this.field = field;
		field.setEditable(true);
		field.setColumns(30);
		field.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		field.setDocument(new JTextFieldLimit(super.getDataField().getColumn().getMaxSize()));

		field.setVisible(true);
		this.add(field);

		this.updateUI();
	}

	/**
	 * The Class JTextFieldLimit, which is what limits the user from inputing stuff that is larger
	 * then the limit
	 */
	 static class JTextFieldLimit extends PlainDocument {

		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = -6946765611454453886L;

		/** The limit as to how much the user can input. */
		private int limit;

		/**
		 * Instantiates a new j text field limit.
		 *
		 * @param limit maximum length that the user can input
		 */
		JTextFieldLimit(int limit) {
			super();
			this.limit = limit;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.text.PlainDocument#insertString(int, java.lang.String,
		 * javax.swing.text.AttributeSet)
		 */
		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			if (str == null)
				return;

			if ((getLength() + str.length()) <= limit) {
				super.insertString(offset, str, attr);
			}
		}
	}
}
