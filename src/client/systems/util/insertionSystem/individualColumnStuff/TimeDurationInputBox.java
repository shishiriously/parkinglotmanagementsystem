package client.systems.util.insertionSystem.individualColumnStuff;

import java.awt.Font;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import client.dataFieldStuff.Column;
import client.dataFieldStuff.DataFieldInputBoxConstructor;
import client.dataFieldStuff.DataField;

/**
 * The Class TimeDurationInputBox, which allows the user to easilt input a time duration
 */
public class TimeDurationInputBox extends DataInputBox {

	/**
	 * returns a constructor which constructs a TimeDurationInputBox
	 *  @returna constructor which constructs a TimeDurationInputBox
	 */
	public static DataFieldInputBoxConstructor constuctDataFieldInputBox() {
		return (DataField s) -> {
			return new TimeDurationInputBox(s);
		};
	}

	/** place where the user inputs the start time. */
	private TimeInputBox start;
	
	/** place that the user inputs the end time */
	private TimeInputBox end;
	
	/** The place the user inputs the start am/pm */
	private RadioButtonDataInputBox startAm;
	
	/** The place the user inputs the start am/pm */
	private RadioButtonDataInputBox endAm;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8408795428310827802L;

	/**
	 * Instantiates a new time duration input box.
	 * @param data the data that the Input Box represents
	 */
	public TimeDurationInputBox(DataField data) {
		super(data, .34);
		JLabel label = new JLabel(data.getColumn().getColumnName());
		label.setHorizontalAlignment(SwingConstants.TRAILING);
		label.setFont(new Font("Times New Roman", Font.PLAIN, 25));

		String[] amAndpmArray = { "am", "pm" };
		ArrayList<String> amAndPm = new ArrayList<String>(Arrays.asList(amAndpmArray));

		start = new TimeInputBox(new DataField(new Column("start time", 5, -1, false, false), null));
		start.getField().setColumns(5);
		end = new TimeInputBox(new DataField(new Column("end time", 5, -1, false, false), null));
		end.getField().setColumns(5);

		startAm = new RadioButtonDataInputBox(new DataField(new Column("", 2, -1, false, false), null), amAndPm);
		endAm = new RadioButtonDataInputBox(new DataField(new Column("", 2, -1, false, false), null), amAndPm);
		this.add(label);
		this.add(start);
		this.add(startAm);
		this.add(end);
		this.add(endAm);
		this.updateUI();
		this.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see java.util.function.Supplier#get()
	 */
	@Override
	public String get() {
		DataInputBox[] boxes = { start, startAm, end, endAm };
		String[] results = new String[boxes.length];

		for (int i = 0; i < boxes.length; i++) {
			results[i] = boxes[i].get();
			if (results[i] == null) {
				return null;
			}
		}
		return results[0] + " " + results[1] + " - " + results[2] + " " + results[3];
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setEditable(boolean)
	 */
	@Override
	public void setEditable(boolean edit) {
		start.setEditable(edit);
		end.setEditable(edit);
		startAm.setEditable(edit);
		endAm.setEditable(edit);

	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setUserInputedText(java.lang.String)
	 */
	@Override
	public void setUserInputedText(String s) {
		String[] splitByAm = s.split(" - ");
		if (splitByAm.length != 2) {
			throw new IllegalArgumentException("error, your input in setText in TimeDurtionInputBox is not valid");
		}
		setTextofPair(splitByAm[0], start, startAm);
		setTextofPair(splitByAm[1], end, endAm);
	}

	/**
	 * Sets the text of a TimeInputBox and RadioButtonDataInput box pair
	 * @param string the string which the pair will be set to
	 * @param start2 the TimeInputBox whichs time will be set
	 * @param startAm2 the RadioButtonDataInputBox whichs am/pm will be set
	 */
	private void setTextofPair(String string, TimeInputBox start2, RadioButtonDataInputBox startAm2) {
		String[] split = string.split(" ");
		if (split.length != 2) {
			throw new IllegalArgumentException("error, your input in setText in TimeDurtionInputBox is not valid");
		}
		start2.setText(split[0]);
		startAm2.setText(split[1]);
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setTextInvisable()
	 */
	@Override
	public void setTextInvisable() {
		start.setTextInvisable();
		end.setTextInvisable();
		startAm.setTextInvisable();
		endAm.setTextInvisable();
	}

	/**
	 * The Class TimeInputBox, which enables the user to input the time in a clear way
	 */
	private static class TimeInputBox extends DefaultDataInputBox {

		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = -7797251257829148839L;

		/**
		 * Instantiates a new time input box.
		 *
		 * @param data the data that the TimeInputBox has
		 */
		public TimeInputBox(DataField data) {
			super(data);
		}

		/* (non-Javadoc)
		 * @see client.systems.util.insertionSystem.individualColumnStuff.DefaultDataInputBox#setUserInputedText(java.lang.String)
		 */
		@Override
		public void setUserInputedText(String s) {
			if (s != null) {
				String dateformat = "HH:mm";
				SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
				dateFormat.setLenient(true);
				try {
					dateFormat.parse(s);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("error, make sure your "
							+ super.getDataField().getColumn().getColumnName() + " matches the following pattern \r\n "
							+ dateformat + " including the colons and the dash");
				}
				super.setUserInputedText(s);
			}
		}
	}

}
