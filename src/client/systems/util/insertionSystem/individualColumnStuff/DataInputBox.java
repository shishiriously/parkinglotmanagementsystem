package client.systems.util.insertionSystem.individualColumnStuff;

import java.util.function.Supplier;

import javax.swing.JPanel;

import client.dataFieldStuff.DataField;

/**
 * this is an abstract class for anything that the user uses to input data into the database. Like say
 * you want the user to input a ID, what you use to enter an id must extend DataInputBox.
 */
public abstract class DataInputBox extends JPanel implements Supplier<String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6151163635099159493L;
	
	/** How large is this thing,  on a scale of 0-1 */
	private double largnessConstant;
	
	/** The data that this box holds/ refers to. */
	private DataField data;

	/**
	 * this is a constcutor for datafield input box, which is basically an input box
	 * for the user to input 1 thing in. like the id, or the date, etc.
	 *
	 * @param data the data
	 * @param largnessConstant            how big your panel is, on a scale of 0-1. DONT PUT THIS NUMBER IN
	 *            YOUR lower level CONSTRUCTOR.
	 */
	public DataInputBox(DataField data, double largnessConstant) {
		this.data = data;
		this.largnessConstant = largnessConstant;
	}

	/**
	 * gets the DataField that this DataInputBox is displaying
	 * @return the DataField that the DataInputBox is displaying
	 */
	public DataField getDataField() {
		return data;
	}

	/**
	 * sets the dataInput box to be editable/uneditable
	 * @param edit is it to be editable or uneditable
	 */
	public abstract void setEditable(boolean edit);
	/**
	 * set the text that the user inputs to the value. like if you want to have the input box to
	 * display a string value before the user inputs anything, this method would be called.
	 * @param s the string value that will be inputed into where the user inputs things
	 */
	public final void setText(String s) {
		if (s == null) {
			setTextInvisable();
			return;
		} else {
			setUserInputedText(s);
		}
	}
	/**
	 * set the text that the user inputs to the value. like if you want to have the input box to
	 * display a string value before the user inputs anything, this method would be called.
	 * NOTE: you dont have to worry about null values, since setText already handles the null value
	 * @param s the string value that will be inputed into where the user inputs things
	 */
	protected abstract void setUserInputedText(String s);

	/**
	 * sets the users input invisable. like they cannot see it anymore
	 */
	public abstract void setTextInvisable();

	/**
	 * sets the text that is stored in the DataField visable. like if you stored "I LIKE GRAPES" in
	 * the DataField, then it would say "I LIKE GRAPES" in the DataInputBox
	 */
	public void setTextVisable() {
		String set = data.getData();
		if (set != null) {
			set = set.trim();
		}
		this.setText(set);
	}

	/**
	 * Gets how big this thing is, on a scale of 0-1
	 *
	 * @return how bing this thing is, on a scale of 0-1
	 */
	public double getLargnessConstant() {
		return largnessConstant;
	}

}
