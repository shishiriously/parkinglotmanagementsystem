package client.systems.util.insertionSystem.individualColumnStuff;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.Border;

import client.dataFieldStuff.DataFieldInputBoxConstructor;
import client.dataFieldStuff.DataField;

/**
 * The Class BigDataInputBox, which is basically a DefaultDataInput box, only it is much bigger and
 * the user can input multiple lines
 */
public class BigDataInputBox extends DataInputBox {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The area that the user inputs using. */
	private JTextArea area;
	
	/** The label that tells the user what it is inputing */
	private JLabel label;

	/**
	 * returns a DataFieldInputBoxConstructor which constructs a BigDataInputBox.
	 * @return a DataFieldInputBoxConstructor which constructs a BigDataInputBox
	 */
	public static DataFieldInputBoxConstructor constuctDataFieldInputBox() {
		return (DataField s) -> {
			return new BigDataInputBox(s);
		};
	}

	/**
	 * Instantiates a new big data input box.
	 * @param data the data that the DataInputBox is inputing
	 */
	public BigDataInputBox(DataField data) {
		super(data, .1);

		this.label = new JLabel(data.getColumn().getColumnName());
		this.label.setHorizontalAlignment(SwingConstants.TRAILING);
		this.label.setFont(new Font("Times New Roman", Font.PLAIN, 25));

		this.area = new JTextArea(5, 50);
		this.area.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		area.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		this.area.setDocument(new DefaultDataInputBox.JTextFieldLimit(super.getDataField().getColumn().getMaxSize()));
		this.setEditable(true);
		this.add(this.label);
		this.add(this.area);
		this.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setEditable(boolean)
	 */
	public void setEditable(boolean edit) {
		this.area.setEditable(edit);
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setUserInputedText(java.lang.String)
	 */
	public void setUserInputedText(String s) {
		area.setText(s);
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setTextInvisable()
	 */
	public void setTextInvisable() {
		area.setText("");
	}

	/**
	 * Gets the area.
	 *
	 * @return the area
	 */
	public JTextArea getArea() {
		return area;
	}

	/* (non-Javadoc)
	 * @see java.util.function.Supplier#get()
	 */
	@Override
	public String get() {
		return area.getText();
	}

	/**
	 * Sets the area that the user inputs using
	 * @param area the new area that the user inputs using
	 */
	public void setArea(JTextArea area) {
		area.setText(this.area.getText());
		this.area.setVisible(false);
		this.remove(this.area);
		this.area = area;
		area.setEditable(true);
		area.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		area.setDocument(new DefaultDataInputBox.JTextFieldLimit(super.getDataField().getColumn().getMaxSize()));
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		area.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		area.setVisible(true);
		this.add(area);

		this.updateUI();
	}

	
}
