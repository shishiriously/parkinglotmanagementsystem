package client.systems.util.insertionSystem.individualColumnStuff;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import client.dataFieldStuff.DataFieldInputBoxConstructor;
import client.dataFieldStuff.DataField;

/**
 * The Class RadioButtonDataInputBox, which is what the user uses to input stuff using radio buttons
 * like if you want the user to interact with several radio button inputs using this class, it is easy to do
 */
public class RadioButtonDataInputBox extends DataInputBox {
	
	/**
	 * returns a DataFieldInputBoxConstructor which constructs a RadioButtonDataInputBox.
	 *
	 * @param buttonNames the tags next to each radioButton
	 * @return a DataFieldInputBoxConstructor which constructs a RadioButtonDataInputBox
	 */
	public static DataFieldInputBoxConstructor constuctDataFieldInputBox(String[] buttonNames) {
		return (DataField s) -> {
			ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(buttonNames));
			return new RadioButtonDataInputBox(s, arrayList);
		};
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8663142290635324307L;
	
	/** The Radio buttons. */
	private final ArrayList<JRadioButton> buttons;
	
	/** The button group, which each radioButton belongs to */
	private final ButtonGroup buttonGroup;

	/**
	 * Instantiates a new radio button data input box
	 * @param data the data that the RadioButtonDataInputBox represents
	 * @param options the options that the user has to input, (1 string for each radio button)
	 */
	public RadioButtonDataInputBox(DataField data, ArrayList<String> options) {
		super(data, .2);
		buttons = new ArrayList<JRadioButton>();
		JLabel label = new JLabel(data.getColumn().getColumnName());
		label.setHorizontalAlignment(SwingConstants.TRAILING);
		label.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		this.add(label);
		label.setVisible(true);

		buttonGroup = new ButtonGroup();
		for (String option : options) {
			JRadioButton currentButton = new JRadioButton(option);
			buttonGroup.add(currentButton);
			buttons.add(currentButton);
			this.add(currentButton);
			currentButton.setVisible(true);
		}
		this.updateUI();
		this.setVisible(true);

	}

	/* (non-Javadoc)
	 * @see java.util.function.Supplier#get()
	 */
	@Override
	public String get() {
		for (JRadioButton current : buttons) {
			if (current.isSelected()) {
				return current.getText();
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setEditable(boolean)
	 */
	@Override
	public void setEditable(boolean edit) {
		for (JRadioButton current : buttons) {
			current.setEnabled(edit);
		}
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setUserInputedText(java.lang.String)
	 */
	@Override
	public void setUserInputedText(String s) {
		for (JRadioButton current : buttons) {
			if (current.getText().equals(s)) {
				onSuccessfulSetText(s, current);
				return;
			}
		}

		throw new IllegalArgumentException(
				"error, the set Text does not match any radio button in RadioButtonDataInputBox, please contact support for help");
	}

	/**
	 * when the string (s) is found to match the radioButtons text, sets the RadioButton to selected
	 * @param s the string value that was selected by the user
	 * @param but the button that is to be selected
	 */
	private void onSuccessfulSetText(String s, JRadioButton but) {
		setTextInvisable();
		but.setSelected(true);
	}

	/* (non-Javadoc)
	 * @see client.systems.util.insertionSystem.individualColumnStuff.DataInputBox#setTextInvisable()
	 */
	@Override
	public void setTextInvisable() {
		buttonGroup.clearSelection();
	}

}
