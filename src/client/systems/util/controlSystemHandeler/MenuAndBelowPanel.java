package client.systems.util.controlSystemHandeler;

import javax.swing.*;

import java.awt.*;

/**
 * this is a class which represents a pannel which will hold a menu bar, and something below the menu bar.  it is
 * expected that a Control System Handeler will manipulate this class to swap the contents when the control system wants
 * to
 */
class MenuAndBelowPanel extends JPanel {
    /**
	 * serial id
	 */
	private static final long serialVersionUID = -6380927526069791287L;
	/**
     * the menu bar which will be above the center panel.
     */
    private MenuBar menuBar;
    /**
     * the panel which will be below the menu bar.
     */
    private JPanel center;


    public MenuAndBelowPanel(MenuBar bar) {
        this.menuBar=bar;
        this.setLayout(new BorderLayout());
        this.add(menuBar,BorderLayout.NORTH);
    }
    /**
     *   this is the method which will reset what panel is being displayed. ie when the center is set, then a new
     *   panel will display in the center.
     */
    public void setCenter(JPanel newCenter){
        if(newCenter==null){
            throw new NullPointerException("error, you tried to set the center of a subsystem to null");
        }
        if(center!=null){
            this.remove(center);
        }
        center=newCenter;
        this.add(newCenter,BorderLayout.CENTER);
        this.updateUI();
    }
}
