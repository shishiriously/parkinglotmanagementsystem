package client.systems.util.controlSystemHandeler;

import javax.swing.*;

import client.communicationToServer.ServerCommunicator;
import client.systems.ControlSystem;
import client.systems.util.ControlSystemWithTitle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * ok this is a fairly complicated class.  This object is basically a system that
 * controls other systems. It creates a menu bar, which has several buttons that activate their respective system
 * when a button is pressed, and deactivate the previously active system.  only one system can be active at a time.
 *
 */
public class ControlSystemHandler extends ControlSystem {
    /**
     * this is the set of control systems which the object can activate and deacativate
     */
    private ArrayList<ControlSystem> controlSystems;
    /**
     * this is the currently active controlSystem, this control system has its veiw visable, and its actionlistners can
     * do stuff.  all of the systems that are not active are not visable, and their actionlisteners do nothing.
     */
    private ControlSystem currentActiveSystem;
    /**
     * this is the menubar, which will activate and deactivate the systems when its buttons are pressed
     */
    private MenuBar menu;
    /**
     * this is the overall veiw, which holds the menuBar, and holds the panel of the currently active system
     * system
     */
    private MenuAndBelowPanel overallVeiw;
    private boolean displayTitles;
    /**
     *
     * @param s the server communicator which it may communicate to the server with.
     * @param name the name of the system
     * @param controlSystems the controlSystems which will be activated and deactivated by the menu bar.
     */
    public ControlSystemHandler(ServerCommunicator s, String name, ArrayList<ControlSystem> controlSystems) {
        super(s,name);
        this.controlSystems = controlSystems;
        displayTitles=true;
    }

    /**CODE MONKEYS!
     * string Kevin = "good";
     * String Juley="Bad";
     *
     *
     * this opens the system, which means that it sets the system up to be usable, and makes the view visable.
     * it does this by setting up the menu bar, and the overall veiw.  it then activates the first control system
     * in the arraylist.
     */
    @Override
    public void open() {
        if(controlSystems.isEmpty()){
            throw new IllegalArgumentException("error, you tried to open a systemHandler with no Systems");
        }
        setupMenuBar();
        setupOverallView();
        swap(controlSystems.get(0));
    }

    /**
     * this sets up the menu bar, which is the panel which holds the buttons that can change what system is active.
     */
    private void setupMenuBar() {
        menu =new MenuBar();
        addBaseSystemActionListener();
    }
    /**
     * this sets up the overall veiw, ie the panel which holds the menu bar, and holds the panel from the single currently
     * active system.
     */
    private void setupOverallView() {
        overallVeiw=new MenuAndBelowPanel(menu);
    }

    /**
     * this adds the action listeners to the menu bar. The actionlisteners that are added to the menu bar swap which
     * system is active, because remember, only one system can be active at a time.
     */
    private void addBaseSystemActionListener() {
        //for all of the control systems that we want to handle...
        for(int i = 0; i< controlSystems.size(); i++){

            ControlSystem controlSystemAt = controlSystems.get(i);
            //create a new jbutton with the title of the system
            //add an action listener which swaps to to the control system.
            ActionListener list=new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    swap(controlSystemAt);
                }
            };
            //add the button to the menu bar
           addButton(list,controlSystemAt.getTitle());
        }

    }

    /**
     * this is the method which swaps which system is active. it takes the currentActiveSystem system, and closes it.
     * it then opens the new system and makes the new control systems' panel visable.
     * @param newControlSystem
     */
    private void swap(ControlSystem newControlSystem) {
    	if(displayTitles) {
    		newControlSystem=new ControlSystemWithTitle(newControlSystem);
    	}
    	try {
        if(currentActiveSystem !=null){
            currentActiveSystem.close();
        }
        currentActiveSystem = newControlSystem;
        newControlSystem.open();
        overallVeiw.setCenter(newControlSystem.getDefaultPanel());
        //sets the defualt panel to visable, just in case the programmer forgot to do that in open.
        newControlSystem.getDefaultPanel().setVisible(true);
    	}catch(Exception e) {
    		JOptionPane.showMessageDialog(null,"Error in either open or close in ControlSystem");
    		e.printStackTrace();
    	}

    }
    /**
     * adds a new button to the menubar above the systems 
     * @param listener the action listener for the new button
     * @param title the text that the button displays
     */
    public void addButton( ActionListener listener,String title) {
    	menu.addButton(listener, title);

    }

    /**
     * returns the overall panel of the system.  this panel includes the menu bar, and the panel of the currently
     * active system.
     * @return a panel which has the menu bar, and the panel of the currently active system.
     */
    @Override
    public JPanel getDefaultPanel() {
        return overallVeiw;
    }

    /**
     * this method activates when the system is closed, ie when this system is no longer active, and another system
     * activates instead. In this case, it closes the currently active system, and sets all relevent pointers to null.
     */
    @Override
    public void close() {
        currentActiveSystem.close();
        currentActiveSystem =null;//just here for memory effiency reasons
        menu=null;
        overallVeiw=null;
    }
    /** 
     * set wheter or not it should display the titles or not. must be done before open is called
     * @param displayTitles whether it should display the titles of the systems or not
     */
	public void setDisplayTitles(boolean displayTitles) {
		this.displayTitles = displayTitles;
	}
    
}
