package client.systems.util.controlSystemHandeler;

import javax.swing.*;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * this class represents a menu bar, which is basically a bunch of buttons which are able to swap which system is active
 * it is using flow layour for now, which is bad I know. we can edit this later to look less terrible.
 */
 public class MenuBar extends JPanel{
    /**
	 * serial id
	 */
	private static final long serialVersionUID = 4009464975687701602L;
	/**the buttons that the user interacts with */
	private ArrayList<JButton> buttons;
    public MenuBar() {
        buttons=new ArrayList<JButton>();
    }

    /**
     * adds a button to the menu.  the menu will now hold one more button.
     * @param but the button which will be added to the menu.
     */
     public void addButton(ActionListener list,String title ){
    	JButton but=new JButton(title);
    	but.addActionListener(list);
    	but.setSize(45, 15);
    	but.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        this.add(but);
        buttons.add(but);
        this.updateUI();
     }
}
