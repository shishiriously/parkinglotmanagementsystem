package client.systems.util.listSystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.function.Consumer;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataField;
import client.dataFieldStuff.DataFieldList;
import client.systems.ControlSystem;


import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ModifyingDataBaseServerFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.*;
/**
 * note: i made it so hidden items cannot be searched using (by the user, not the code) and
 * made it so hidden items will not show in the results of a search. 
 * 
 * @author brevin
 *
 */

public class ListSystem extends ControlSystem{
	
	private ArrayList<DataFieldList> rows;
	private DataFieldList search;
	private ListView view;
	
	public ListSystem(ServerCommunicator s, DataFieldList whatToSearchUsing) {
		super(s, "search for "+whatToSearchUsing.getTable().getTitle());
		search = whatToSearchUsing.getListWithoutHiddenItems();
	}

	

	@Override
	public void open() {
		view = new ListView(search, super.getTitle());
		view.setVisible(true);
		addOurListeners();
		
	}
	
	private void addOurListeners()
	{
		
		
		view.addListListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if(view.getSelectedIndex()!=-1) {
				 view.fillDescription(view.getSelectedIndex());
				}
				
			}});
		
		Consumer<ArrayList<DataFieldList>> searcher = this::afterSearchingDataBase;
		
		view.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int selectedSearchOption = view.getSelectedSearchOption();
				DataFieldList searchItem = new DataFieldList(search.getTable());
				
				
				String searching=view.getSearchText();
				
				if(searching.equals("")) {
					searching=null;
				}
				
				DataField temp = new DataField(search.get(selectedSearchOption).getColumn(), searching);
				
				searchItem.add(temp);
				//This is going to be a search method
				getServ().sendStuffToServer(searcher, null, new ModifyingDataBaseServerFunction(new SearchDataBaseFunction(searchItem)));
				
			}
			
		});
	}
	
	public void afterSearchingDataBase(ArrayList<DataFieldList> result) {
//		for(int i=0;i<result.size();i++) {
//			result.set(i, result.get(i).getListWithoutHiddenItems());
//		}
		rows = result;
		view.fillList(result);
	}

	@Override
	public JPanel getDefaultPanel() {
		// TODO Auto-generated method stub
		return view;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		view.setVisible(false);
		
	}
	
	protected ListView getView() {return view;}
	protected DataFieldList getSearch() {return search;}
	protected void setView(ListView v) 
	{
		view = v;
		addOurListeners();
	}
	protected ArrayList<DataFieldList> getDataFieldLists(){return rows;}

}
