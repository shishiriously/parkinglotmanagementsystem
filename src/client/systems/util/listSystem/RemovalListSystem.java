package client.systems.util.listSystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.function.Consumer;

import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;
import client.systems.util.insertionSystem.ModificationSystem;
import client.systems.util.jFrameStuff.SystemInFrameHandeler;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.ModifyingDataBaseServerFunction;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.*;

public class RemovalListSystem extends ListSystem
{
	private SystemInFrameHandeler frame;

	public RemovalListSystem(ServerCommunicator s, DataFieldList whatToSearchUsing) {
		super(s, whatToSearchUsing);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void open()
	{
		RemovalListView theView = new RemovalListView(super.getSearch(), super.getTitle());
		super.open();
		super.setView(theView);
		frame=new SystemInFrameHandeler();
		Consumer<ArrayList<DataFieldList>> onDeleteSuccess=this::onDeleteSucess;
		theView.addRemovalListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				DataFieldList toDelete = new DataFieldList(getDataFieldLists().get(theView.getSelectedIndex()));
//				getDataFieldLists().remove(theView.getSelectedIndex());
//				theView.fillList(getDataFieldLists());
				
				//This is going to be a delete method
				getServ().sendStuffToServer(onDeleteSuccess, null, new ModifyingDataBaseServerFunction(new DeletingDataBaseFunction(toDelete)));
				//theView.deselectItem();
				//theView.fillList(getDataFieldLists());
			}
		});
		ServerCommunicator serv=super.getServ();
		theView.addModifyListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				DataFieldList modify = new DataFieldList(getDataFieldLists().get(theView.getSelectedIndex()));
				modify=modify.getListWithoutHiddenItems();
				
				ModificationSystem modSys=new ModificationSystem(serv, modify);
				frame.addSystem(modSys);
			}
			
		});
		
	}
	public void onDeleteSucess(ArrayList<DataFieldList> list) {
		
		RemovalListView theView=null;
		try {
			 theView=(RemovalListView)super.getView();
		}catch(ClassCastException e) {
			throw new IllegalArgumentException("errror, you set the RemovalListView of a removalListSystem to a non RemovalListView");
		}
		
		theView.getRemoveButton().setEnabled(false);
		theView.getModifyButton().setEnabled(false);
		theView.updateUI();
		

		getDataFieldLists().remove(theView.getSelectedIndex());
		theView.updateList(getDataFieldLists());
	}
	
	@Override
	public void close() {
		super.close();
		frame.close();
	}

}
