package client.systems.util.listSystem;

import client.dataFieldStuff.DataFieldList;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class RemovalListView extends ListView
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8379804535189670669L;
	private JButton removeButton, modifyButton;

	protected RemovalListView(DataFieldList whichhFromatToUse, String Title) {
		super(whichhFromatToUse, Title);
		
		removeButton = new JButton("Remove");
		removeButton.setEnabled(false);
		removeButton.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		removeButton.setBounds(1001, 72, 221, 49);
		add(removeButton);
		
		modifyButton = new JButton("Modify");
		modifyButton.setEnabled(false);
		modifyButton.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		modifyButton.setBounds(1240, 72, 221, 49);
		add(modifyButton);
	}
	
	@Override
	protected void fillDescription(int index)
	{
		super.fillDescription(index);
		removeButton.setEnabled(true);
		modifyButton.setEnabled(true);
	}
	
	protected void addRemovalListener(ActionListener listener)
	{
		removeButton.addActionListener(listener);
	}
	
	protected JButton getRemoveButton() {return removeButton;}
	
	
	
	protected void deselectItem()
	{
		super.getJList().clearSelection();
	}
	
	protected void addModifyListener(ActionListener m)
	{
		modifyButton.addActionListener(m);
	}
	
	protected JButton getModifyButton() {return modifyButton;}
	
}
