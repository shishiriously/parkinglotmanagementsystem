package client.systems.util.listSystem;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import client.dataFieldStuff.DataFieldList;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.border.BevelBorder;

public class ListView extends JPanel {

	private ArrayList<DataFieldList> rows;
	private DataFieldList format;
	private JTextField searchBar;
	private JTextArea selectionDescription;
	private JList selectionOptions;
	private JButton searchButton;
	private Vector<String> listItems;
	private JComboBox searchOptions;
	private JScrollPane scrollPane;

	protected ListView(DataFieldList whichhFromatToUse, String Title) {
		this.setSize(1920, 904);
		this.setMinimumSize(new Dimension(1920, 904));

		this.setLayout(null);
		format = new DataFieldList(whichhFromatToUse);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(49, 134, 850, 742);
		add(scrollPane);

		selectionOptions = new JList();
		selectionOptions.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		scrollPane.setViewportView(selectionOptions);

		searchBar = new JTextField();
		searchBar.setText("Search.....");
		searchBar.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		searchBar.setBounds(49, 10, 617, 49);
		add(searchBar);
		searchBar.setColumns(10);

		searchButton = new JButton("Search");
		searchButton.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		searchButton.setBounds(678, 10, 221, 49);
		add(searchButton);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(1001, 134, 856, 742);
		add(scrollPane_1);

		selectionDescription = new JTextArea();
		scrollPane_1.setViewportView(selectionDescription);
		selectionDescription.setFont(new Font("Times New Roman", Font.PLAIN, 25));

		createSearchOptionMenu();

		listItems = new Vector<String>();

	}

	/**
	 * Fills the searchOptions drop down menu with the names of the different
	 * columns for the specific entity type
	 */
	private void createSearchOptionMenu() {
		searchOptions = new JComboBox();
		searchOptions.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		searchOptions.setBounds(49, 72, 850, 49);
		add(searchOptions);

		if (format != null) {
			for (int i = 0; i < format.size(); i++) {
				searchOptions.addItem(format.get(i).getColumn().getColumnName());
			}
		}

	}

	/**
	 * Fills the JList with the Data Contained in the key attributes of the
	 * DataField of each DataFieldList
	 */
	protected void fillList(ArrayList<DataFieldList> data) {
		for (int i = 0; i < data.size(); i++)
			rows = new ArrayList<DataFieldList>(data);
		listItems = new Vector<String>();
		if (data != null) {
			for (int i = 0; i < data.size(); i++) {
				String temp = "";
				for (int j = 0; j < data.get(i).size(); j++) {
					if (data.get(i).get(j).getColumn().isKey()) {
						System.out.println("Is key \n");
						temp += data.get(i).get(j).getColumn().getColumnName() + ": " + data.get(i).get(j).getData();
						// listItems.add(temp);
					}
				}

				listItems.add(temp);
			}

			for (int i = 0; i < listItems.size(); i++)
				System.out.println(listItems.get(i) + " from list items");
			this.selectionOptions.setListData(listItems);
			// this.selectionOptions = new JList(listItems);
			// this.scrollPane.setViewportView(selectionOptions);
			// selectionOptions.setVisible(true);
		} else {
			System.out.println("Null");
			selectionOptions = new JList();
			rows = null;
		}
	}

	/**
	 * Adds an ActionListener to the search button
	 * 
	 * @param listener
	 */
	protected void addActionListener(ActionListener listener) {
		searchButton.addActionListener(listener);
	}

	/**
	 * Adds a ListSelectionListener to selectionOptions
	 * 
	 * @param listener
	 */
	protected void addListListener(ListSelectionListener listener) {
		selectionOptions.addListSelectionListener(listener);
	}

	/**
	 * Adds an ActionListener to searchOptions
	 * 
	 * @param listener
	 */
	protected void addSearchOptionActionListener(ActionListener listener) {
		searchOptions.addActionListener(listener);
	}

	/**
	 * Gets the index number of the selected search option
	 * 
	 * @return
	 */
	protected int getSelectedSearchOption() {
		return searchOptions.getSelectedIndex();
	}

	protected String getSearchText() {
		return searchBar.getText();
	}

	/**
	 * Gets the index number of the list item selected by the user
	 * 
	 * @return the selected index
	 */
	protected int getSelectedIndex() {
		return selectionOptions.getSelectedIndex();
	}

	/**
	 * Fills the description box with the information contained in the selected
	 * DataFieldList
	 * 
	 * @param index:
	 *            index of the selected item from the list of search results
	 */
	protected void fillDescription(int index) {
		String temp = "";
		if (index >= 0) {
			for (int i = 0; i < rows.get(index).size(); i++) {
				if (!rows.get(index).get(i).getColumn().isHiddenFromSearches()) {
					temp += rows.get(index).get(i).getColumn().getColumnName() + ": " + rows.get(index).get(i).getData()
							+ "\n";
				}
			}
		}

		selectionDescription.setText(temp);
	}

	/**
	 * Search through each DataField in each DataFieldList to see if any entries
	 * matches the text entered in searchBar. If a match is found the index that
	 * correspounds to the found DataField is sent to fillDescription to fill the
	 * description box with the found information.
	 */
	protected void search() {
		String search = searchBar.getText();
		for (int i = 0; i < rows.size(); i++) {
			for (int j = 0; j < rows.get(i).size(); j++) {
				if (search.equals(rows.get(i).get(j).getData())) {
					fillDescription(i);
					return;
				}
			}
		}
	}

	protected void setDescription(String set) {
		selectionDescription.setText(set);
	}

	protected JList getJList() {
		return selectionOptions;
	}

	protected void updateList(ArrayList<DataFieldList> data) {
		setDescription("");
		fillList(data);
	}
}
