package client.systems.util;

import javax.swing.*;

import client.communicationToServer.ServerCommunicator;
import client.systems.ControlSystem;

/**
 * Step 8.
 *
 *this is the simplest possible control system, as it just has a veiw.
 *
 * think that "open" is when the system gets activated and whatever you want on the screen appears, and "close" is when
 * the system is deactivated/ the veiw from the system is no longer visable.
 *
 * REQUIRES ALL LISTS TO BE SET/SYNCRONIZED BEFORE ADDING TO THE SYSTEM.
 */
public class SimpleControlSystem extends ControlSystem {
	JPanel pannel;
	public SimpleControlSystem(ServerCommunicator serv, String title, JPanel pannel) {
		super(serv,title);
		this.pannel=pannel;
	}

	/**
	 * open is when the system is activated; so when the veiw and the button appears on sceen.  In this case, when
	 * veiw is activated, the jlist and the button appears on screen.
	 */
	@Override
	public void open() {
		pannel.setVisible(true);
	}

	/**
	 * this is a getter for the jpanel that the system outputs.  ie in this case, it is veiw.
	 * @return
	 */
	@Override
	public JPanel getDefaultPanel() {
		return pannel;
	}

	/**
	 * this activates whenever the system is closed, so when the veiw of the system is gon, and another
	 * system is activated-ie its view is onscreen, then it activates this method.
	 */
	@Override
	public void close() {
		pannel.setVisible(false);
	}
}
