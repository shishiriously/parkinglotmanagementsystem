package client.systems.util.jFrameStuff;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import client.systems.ControlSystem;

/**
 * The Class SystemJFrame, which is a JFrame that holds a single system
 */
public class SystemJFrame extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1178816631021987512L;
	
	/** The system that it holds. */
	private ControlSystem system;
	
	
	/**
	 * Instantiates a new system J frame.
	 *
	 * @param system the system that it will hold
	 * @throws HeadlessException the headless exception
	 */
	public SystemJFrame(ControlSystem system) throws HeadlessException {
		super();
		this.system = system;
		this.addWindowListener(createWindowListener());
		
			system.open();
			system.getDefaultPanel().setVisible(true);
			this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
			this.setTitle(system.getTitle());
			
			this.getContentPane().add(system.getDefaultPanel());
			
			this.setSize(new Dimension(1920,1080));
			this.setVisible(true);
	}

	/**
	 * Creates the window listener which will close the system it has when the Jframe closes
	 * @return the window listener which will close the system it has when the Jframe closes
	 */
	private WindowListener createWindowListener() {
		return new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) { 
		     close();  
		}
	};
	}
	
	/**
	 * Close.
	 */
	private void close() {
	if(system!=null) {
		system.close();
	}
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public ControlSystem getSystem() {
		return system;
	}
	
}
