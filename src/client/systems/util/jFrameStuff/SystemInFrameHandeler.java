package client.systems.util.jFrameStuff;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import client.systems.ControlSystem;

/**
 * The Class SystemInFrameHandeler. which keeps track of a bunch of SystemJFrames, ie systems that
 * are in another JFrame. It handles closing these systems when the system closes
 */
public class SystemInFrameHandeler{
	
	/** The systems. */
	ArrayList<SystemJFrame> systems;
	
	/**
	 * Instantiates a new system in frame handeler.
	 */
	public SystemInFrameHandeler() {
		systems=new ArrayList<SystemJFrame>();
	}

	/**
	 * creates a new Jframe, so that the ControlSystem is displayed in the Frame
	 * @param s the s controlSystem which will be displayed in the frame
	 */
	public void addSystem(ControlSystem s) {
		SystemJFrame added=new SystemJFrame(s);
		added.addWindowListener(createWindowListener(added));
		systems.add(added);
	}
	
	/**
	 * Creates the window listener which removes the system from The handelers arrayList when
	 * the window closes
	 * @param s the s the system which will get the windowlistner, which removes the system from the 
	 * handelers arrayList
	 * @return the window listener which removes the system from this handeler whenever the window closes
	 */
	private WindowListener createWindowListener(SystemJFrame s) {
		return new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) { 
		    	systems.remove(s);
		}
	};
	}
	
	
	/**
	 * Closes each Jframe that it controles.
	 */
	public void close() {

		for(int i=0;i<systems.size();i++) {
			
			systems.get(i).dispatchEvent(new WindowEvent(systems.get(i), WindowEvent.WINDOW_CLOSING));
		}
	}
	
}
