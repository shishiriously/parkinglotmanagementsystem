package client.systems.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import client.communicationToServer.ClientsCommunicationSystem;
import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;
import client.dataFieldStuff.TableCollection;
import client.systems.ControlSystem;
import client.systems.util.insertionSystem.InsertionSystem;
import client.systems.util.jFrameStuff.SystemJFrame;
// TODO: Auto-generated Javadoc

/**
 * this class is basically a controlSystem which also displays its title above it.
 *
 * @author brere
 */
public class ControlSystemWithTitle extends ControlSystem {
	
	/**
	 * Instantiates a new control system with title.
	 *
	 * @param system the system
	 */
	public ControlSystemWithTitle(ControlSystem system) {
		super(system.getServ(), system.getTitle());
		titleSize=35;
		this.system=system;
	}
	
	/** The title which is displayed above the system. */
	private JPanel title;
	
	/** The outward panel, which encapsulates the title, and the ControlSystem below it */
	private	JPanel outward;
	
	/** The ControlSystem that it holds. */
	private ControlSystem system;
	
	/** The title size. */
	private int titleSize;
	
	/* (non-Javadoc)
	 * @see client.systems.ControlSystem#open()
	 */
	@Override
	public void open() {
		system.open();
		title=new JPanel();
		title.setLayout(new BorderLayout());


		JLabel lblNewLabel = new JLabel(this.getTitle());
		lblNewLabel.setHorizontalAlignment(JLabel.CENTER);

		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, titleSize));
		title.add(lblNewLabel,BorderLayout.CENTER);

		
		outward=new JPanel();
		outward.setLayout(new GridBagLayout());
		title.setBackground(Color.CYAN);
//		title.size

		GridBagConstraints cons=new GridBagConstraints();
		cons.weighty=.05;
		cons.weightx=1;
		cons.fill=GridBagConstraints.BOTH;
		cons.gridx=0;
		cons.gridy=0;
		
		
		
		outward.add(title,cons);
		
		
		cons.weighty=0.7;
		cons.gridy=1;
		
		system.getDefaultPanel().setVisible(true);
		
	//	system.getDefaultPanel().setBackground(new Color(0,0,0));
		outward.add(system.getDefaultPanel(), cons);
		
		
		outward.updateUI();
		outward.setVisible(true);
		
	}
	
	/* (non-Javadoc)
	 * @see client.systems.ControlSystem#getDefaultPanel()
	 */
	@Override
	public JPanel getDefaultPanel() {
		// TODO Auto-generated method stub
		return outward;
	}
	
	/* (non-Javadoc)
	 * @see client.systems.ControlSystem#close()
	 */
	@Override
	public void close() {
		outward.removeAll();
		outward.setVisible(false);
		
		title.setVisible(false);
		title=null;
		outward=null;
		system.close();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ServerCommunicator serv=(new ClientsCommunicationSystem()).getComunicator(); 
		ArrayList<DataFieldList> list=new ArrayList<DataFieldList>();
		list.add(TableCollection.USERTABLE.table.getAsDataFieldList());
		list.add(TableCollection.OWNERTABLE.table.getAsDataFieldList());
		list.add(TableCollection.DISPUTETABLE.table.getAsDataFieldList());
		list.add(TableCollection.DISPUTETABLE.table.getAsDataFieldList());
		list.add(TableCollection.DISPUTETABLE.table.getAsDataFieldList());
		list.add(TableCollection.DISPUTETABLE.table.getAsDataFieldList());
		list.add(TableCollection.DISPUTETABLE.table.getAsDataFieldList());

		InsertionSystem s=new InsertionSystem(serv,list);
		ControlSystemWithTitle title=new ControlSystemWithTitle(s);
		
		@SuppressWarnings("unused")
		SystemJFrame j=new SystemJFrame(title);
		
		
		
//		Column.ConstuctorOfDataFeildInputBox boxConstructor=TableCollection.USERTABLE.ID.getUserInputBoxConstructor();
//		DataFieldInputBox box=boxConstructor.constructDataFieldInputBox(new DataField(TableCollection.USERTABLE.ID,null));
//		JFrame j=new JFrame();
//		j.getContentPane().add(box);
//		j.setVisible(true);
	}
	
	
}	

