package client.systems.util;

import java.awt.BorderLayout;

import javax.swing.JPanel;

/**
 * this is a JPanel that can swap between many subpanels easily.
 *
 * @author brere
 */
public class SwappingPanel extends JPanel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2079667081338605381L;
	
	/** The current. */
	JPanel current;
	
	/**
	 * Swaps the visable panel with the new panel
	 *
	 * @param panel the panel which will be visable.
	 */
	public void swap(JPanel panel){
		if(current!=null) {
			current.setVisible(false);
			this.remove(current);
		}
		
		current=panel;
		this.add(panel,BorderLayout.CENTER);
		this.repaint();
		this.updateUI();
	}
	
	/**
	 * Instantiates a new swapping panel.
	 *
	 * @param current the first panel which whill be visable in the swapping pannel
	 */
	public SwappingPanel(JPanel current) {
		super();
		this.setLayout(new BorderLayout());
		this.swap(current);
	}
}
