package client.systems;


import javax.swing.*;
import client.communicationToServer.ServerCommunicator;
import client.dataFieldStuff.DataFieldList;

/**
 * This is a system, which is basically a single "mvc" system which has a model, veiw and controller. some systems also
 * dont have a model, and are designed to swap between multiple views. The "SystemHandeler" in 3.0 is a system which is designed
 * to swap between several sub-systems, using a top down menu.
 * ie a login screen would probably be another system which which would swap between a administrator screen
 * and a login screen.
 *
 * Since the last version, I added a title to it. Its basically what you would expect, just a name for the system.
 */
public abstract class ControlSystem {
	private static DataFieldList userData;
    /**what communicates with the server*/
    private ServerCommunicator serv;
    public ServerCommunicator getServ() {
		return serv;
	}

	/**just the name of the system*/
    private String title;

    public ControlSystem(ServerCommunicator s, String title){
        serv=s;
        this.title=title;
    }
    /**
     * this happens every time you "open" a system. like when the system is opened its view is visable, and its buttons
     * can do stuff. normally this is in instantiation, but if it was in instantiation, then making a general class is
     * impossible
     */
    public abstract void open();
    /**
     * this is meant to reaturn the panel that the System starts with.  ie when a system opens, what is the first thing
     * that you see?
     * @return returns the panel that the system starts with. ie the first thing you see when the system opens.
     */
    public abstract JPanel getDefaultPanel();
    /**
     * this happens any time you close a system.  like when a diffrent system is opened, and this system does not want to
     * be visable any more, then the system would close.
     */
    public abstract void close();
    /**
     * get the title of the system
     * @return the title of the system
     */
    public String getTitle(){
        return title;
    }
    /**
     * get the users data, like the data which pretains to the current user
     * @return the users data, like the data which pretains to the current user
     */
	protected DataFieldList getUserData() {
		return userData;
	}
	/**
	 * sets the user data to the inputed data
	 * @param userData
	 */
	protected static void setUserData(DataFieldList userData) {
		ControlSystem.userData = userData;
	}
}
