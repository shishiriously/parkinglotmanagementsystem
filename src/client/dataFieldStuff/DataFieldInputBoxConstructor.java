package client.dataFieldStuff;

import java.io.Serializable;
import java.util.function.Supplier;

import javax.swing.JPasswordField;

import client.systems.util.insertionSystem.individualColumnStuff.DefaultDataInputBox;

/**
 * constuctor of datafieldInputbox is a weird concept.  basically we want the column objects to be seriazable, but 
 * we also want them to know what view the user needs to input data. Because of this, we basically pass the constructor as a method
 * with the inputed values already in it, except the datafield. The datafield then gets inputed by the insertionview, so it all works.
 * 
 * note: it works a bit strangely, as it interacts with some other systems weirdly. If the supplier of string is not an instance of DataInput box, it will not be enterable by the user.
 * There must be an outside mechanism for getting it then, which does not rely on the insertionview. the get() method will be acted on when the user presses add in the insertionview. 
 * 
 * to get the constructorofDataFeildInputBox, please use the constuctDataFieldInputBox static method in each DataFieldInputBox extension.
 * @author brere
 *
 */
public  interface DataFieldInputBoxConstructor extends Serializable{
	/**
	 * constructs a dataFieldInputBox (if it wants to be visable), or a regular Supplier<String> (if it wants to be invisable)
	 * which's get values will be copied into dataFields.
	 * @param d the datafield It will use to construct a dataFieldInputBox
	 * @return Supplier<String> that will supply a string for a datafield
	 */
	public Supplier<String> constructDataFieldInputBox(DataField d);
	/**
	 * this is a ConstructorOfDataFeildInputSupplier, which returns a supplier, whos get() method would return the Inputed DataFields data
	 * @param d the dataField whose data will be returned by the supplier
	 * @return a supplier, which will return the DataFields data.
	 */
	public static Supplier<String> createSavedDataReturner(DataField d){
			return ()->{return d.getData();};
		}
	/**
	 * this method is a constructorOfDataFieldInputSupplier which is basically a defualtDataInputBox, but 
	 * is a password
	 * @param d the datafield that will be used to construct a DefualtDataInputBox
	 * @return DefaultDataInputBox that is in password mode
	 */
	public static Supplier<String> createLoginPasswordSupplier(DataField d) {
		
			JPasswordField paswordfield=new JPasswordField(10);
			paswordfield.setEchoChar('*');
			//change this later. 
			DefaultDataInputBox password=new DefaultDataInputBox(d);
			password.setField(paswordfield);
			return password;
		
	}
	/**
	 * this method creates a constructor method, which implements ConstructorOfDataFieldSupplier. this constructor returns a supplier, which returns the string inputed in the argument.
	 * @param s the string which the end ConstructorOfDataFieldInputSupplier's outputed supplier will output.
	 * @return a ConstuctorOfDataFeildInputSupplier which will return a supplier, which will return the inputed argument. 
	 */
	public static DataFieldInputBoxConstructor createSetStringSupplierConstructor(String s){
		return(DataField d)->{	
			return()-> {return s; };
		};
	}
	
	}
	



