package client.dataFieldStuff;

import java.io.Serializable;
import java.util.ArrayList;

import client.systems.util.insertionSystem.individualColumnStuff.DefaultDataInputBox;

/**
 * The Class Column, which basically represents a column in the database. It has several attributes which
 * represent the parts in the database, like the size of the data, the column number it is, if it is a key, etc.
 */

//TODO split this class in two, to seperate the database stuff from the visual stuff.
public class Column implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8555098050346472465L;
	
	/** The Constant MAXTITLESIZE, which is the maximum size a title in the database can be. */
	private static final int MAXTITLESIZE = 20;
	/** The label, which is the name of the column. */
	private final String columnName;

	/** The column number, like 1,2,3,4,5 etc for what number of column it is in */
	private final int columnNumber;
	/**
	 * The max size. which is the maximum number of characters that goes into the
	 * database
	 */
	private final int maxSize;
	
	/**  if the column cannot be null */
	private final boolean isNonNULL;
	
	/** is the column a key. */
	private final boolean isKey;
	
	/** The checks for user input, ie the sets of conditions the inputted string IS REQUIRED to adhere to */
	private ArrayList<CheckerOfUserInput> checksForUserInput;
	
	/** is the column hidden from searches, like should it apear in searches */
	private boolean isHiddenFromSearches;

	/** The table which it belongs tos title. */
	private String tableWhichItBelongsTosTitle;

	/** The Constructor for the JPanel that the user will interact with for inputing this data (probably should be in another class or something). */
	private DataFieldInputBoxConstructor boxConstructor;

	/**
	 * Instantiates a new user label.
	 * @param columnName            the  column name
	 * @param maxSize            the max size of the column
	 * @param columnNumber        the column number
	 * @param isNonNull 		is the column non null
	 * @param isKey is the column a key
	 */
	public Column(String columnName, int maxSize, int columnNumber, boolean isNonNull, boolean isKey) {
		checkColumnTitle(columnName);

		this.columnName = columnName;
		this.columnNumber = columnNumber;
		this.maxSize = maxSize;
		this.isNonNULL = isNonNull;
		this.isKey = isKey;
		this.checksForUserInput = new ArrayList<CheckerOfUserInput>();
		this.initializeChecks();
		this.boxConstructor = DefaultDataInputBox.constuctDataFieldInputBox();
		this.isHiddenFromSearches = false;
	}

	/**
	 * Initializes the defualt checks.
	 */
	private void initializeChecks() {
		this.addCheck(CheckerOfUserInput::checkIfColumnIsIllegallyNull);
		this.addCheck(CheckerOfUserInput::checkIfInputIsTooBig);
	}

	/**
	 * Instantiates a new column, copying the inputed column, but seting the column number to i
	 * @param col the column which will be copied
	 * @param i the column number
	 */
	public Column(Column col, int i) {
		this.columnName = new String(col.columnName);
		this.columnNumber = i;
		this.maxSize = col.maxSize;
		this.isNonNULL = col.isNonNULL;
		this.isKey = col.isKey;
		this.tableWhichItBelongsTosTitle = col.tableWhichItBelongsTosTitle;
		this.checksForUserInput = new ArrayList<CheckerOfUserInput>(col.checksForUserInput);
		this.boxConstructor = col.boxConstructor;
		this.isHiddenFromSearches = col.isHiddenFromSearches;
	}

	/**
	 * this is basically clone, but it sets the column number to i. Its a copy constructor that works with polymorphism,
	 * @param i the column number
	 * @return a copy of this column, with a new column number.
	 */
	public Column copy(int i) {
		return new Column(this, i);
	}

	/**
	 * Checks the inputed title if it is too big. 
	 * @param columnName the colum name for this column, which it will check if it is too big.
	 */
	private void checkColumnTitle(String columnName) {
		if (columnName.length() > MAXTITLESIZE) {
			throw new IllegalArgumentException("error, you made a columns title too big");
		}
	}

	/**
	 * Checks the user input, to see if it is acceptable
	 * @param s the user input that will be checked if it is acceptable
	 * @throws IllegalArgumentException when the inputed string is illegal. ie if the string is not a valid input into the database
	 */
	public void checkUserInput(String s) throws IllegalArgumentException {
		if (s == null && (!this.isNonNULL)) {
			return;
		}
		for (int i = 0; i < checksForUserInput.size(); i++) {
			checksForUserInput.get(i).checkInput(s, this);
		}
	}

	/**
	 * Adds another condition that the string must apply by. ie if the checkerOfUserInput is CheckerOfUserInput::checkcheckIfColumnIsADouble
	 * then the user must input a double
	 * @param checker the method which checks if the string fits its conditions
	 */
	public void addCheck(CheckerOfUserInput checker) {
		this.checksForUserInput.add(checker);
	}

	/**
	 * returns the name of the column
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * returns the number that the column is
	 * @return the column number
	 */
	public int getColumnNumber() {
		return columnNumber;
	}

	/**
	 * Gets the max size of a column input
	 * @return the max size of a column input
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/**
	 * is the column non null (ie if true, it cannot be null)
	 * @return  is the column non null (ie if true, it cannot be null)
	 */
	public boolean isNonNull() {
		return isNonNULL;
	}

	/**
	 * checks if the column name and max size are the same.
	 * @param col the column it is comparing to
	 * @return if the column has the same name and max size
	 */
	public boolean equals(Column col) {
		if(col==null) {
			return false;
		}
		return this.columnName.equals(col.columnName) && this.maxSize == col.maxSize;
	}

	/**
	 * Checks if column is key.
	 * @return true, if is key
	 */
	public boolean isKey() {
		return isKey;
	}

	/**
	 * Gets the data base column name, ie the name but with only characters, and all characters
	 * are uppercase letters.
	 * @return the data base column name
	 */
	public String getDataBaseColumnName() {
		return columnName.replaceAll("[^a-zA-Z]", "").toUpperCase();
	}

	/**
	 * Gets the table which the column belongs to.
	 * @return the table which it belongs to
	 */
	public Table getTableWhichItBelongsTo() {
		return TableCollection.getTableByName(tableWhichItBelongsTosTitle);
	}

	/**
	 * Sets the table which it belongs to.
	 * @param tableWhichItBelongsTo the new table which it belongs to
	 */
	protected void setTableWhichItBelongsTo(Table tableWhichItBelongsTo) {
		this.tableWhichItBelongsTosTitle = tableWhichItBelongsTo.getTitle();
	}

	/**
	 * Gets the checks for user input. ie the things that check if a user input is valid or not
	 * @return the checks for user input
	 */
	protected ArrayList<CheckerOfUserInput> getChecksForUserInput() {
		return checksForUserInput;
	}

	/**
	 * Sets the checks for user input, ie the things that check if a user input is valid or not
	 * @param checksForUserInput the new checks for user input
	 */
	protected void setChecksForUserInput(ArrayList<CheckerOfUserInput> checksForUserInput) {
		this.checksForUserInput = checksForUserInput;
	}

	/**
	 * Gets the user input box constructor, ie the thing that constructs the JPanel which the user interacts with
	 * to enter the value of the column.
	 * @return the user input box constructor
	 */
	public DataFieldInputBoxConstructor getUserInputBoxConstructor() {
		return boxConstructor;
	}

	/**
	 * Sets the user input box constructor, ie the thing that constructs the JPanel which the user interacts with
	 * to enter the value of the column.
	 * @param boxConstructor the new user input box constructor
	 */
	public void setUserInputBoxConstructor(DataFieldInputBoxConstructor boxConstructor) {
		this.boxConstructor = boxConstructor;
	}

	/**
	 * Checks if the column is hidden from search results
	 * @return true, if is hidden from searches
	 */
	public boolean isHiddenFromSearches() {
		return isHiddenFromSearches;
	}

	/**
	 * Sets if the colum is hidden from search results.
	 * @param isHiddenFromSearches the new hidden from searches
	 */
	public void setHiddenFromSearches(boolean isHiddenFromSearches) {
		this.isHiddenFromSearches = isHiddenFromSearches;
	}

}