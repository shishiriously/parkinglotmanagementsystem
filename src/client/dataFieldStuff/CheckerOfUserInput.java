package client.dataFieldStuff;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Interface CheckerOfUserInput, which is an interface for methods which check if a string is 
 * an acceptable input for the given column. you should pass these methods using method refrences 
 * to make sure the user inputs correct things into the database
 * 
 * REQUIRES THE METHOD TO THROW AN ILLEGALARGUMENTEXCEPTION IF THE STRING IS NOT ACCEPTABLE
 */
public interface CheckerOfUserInput extends Serializable {
	
	/**
	 * Check user input to see if it is a valid input
	 * @param s the user input that is being checked
	 * @param col the column that the user input belongs to
	 * @throws IllegalArgumentException when the user input was not valid
	 */
	public void checkInput(String s, Column col) throws IllegalArgumentException;

	/**
	 * checks if the string is null illegally.This is automatically added to each
	 * column object.
	 * @param s the s string which will be checked if it is illegally null
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfColumnIsIllegallyNull(String s, Column col) {	
		if (s == null) {
			if (col.isNonNull()) {
				throw new IllegalArgumentException(" error, please fill out " + col.getColumnName());
			}
		}
	}

	/**
	 * this checks if the String input is too big. This is automatically added to
	 * each column object
	 * 
	 * @param s the s string which will be checked if it is too big
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfInputIsTooBig(String s, Column col) {
		if (s.length() > col.getMaxSize()) {
			throw new IllegalArgumentException("error, your " + col.getColumnName() + " input is too big, "
					+ "\r\n please make sure it is less than " + col.getMaxSize() + " long");
		}
	}

	/**
	 * Check if column is A double (like a number with a decimal).
	 *
	 * @param s the s
	 * @param col the col
	 */
	public static void checkIfColumnIsADouble(String s, Column col) {
		try {
			Double.parseDouble(s);
		} catch (NumberFormatException nfe) {
			throw new IllegalArgumentException(
					"error, your input for " + col.getColumnName() + " is not a number. please enter a number");
		}
	}

	/**
	 * Check if column is an integer.
	 *
	 * @param s the s string which will be checked if it is an intiger
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfColumnIsAnInteger(String s, Column col) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			throw new IllegalArgumentException(
					"error, your input for " + col.getColumnName() + " is not a valid number. Please enter a number");
		}
	}

	/**
	 * Check if column is A valid email.
	 *
	 * @param s the s string which will be checked if it is a valid email
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfColumnIsAValidEmail(String s, Column col) {

		// code skillfully borrowed from
		// https://stackoverflow.com/questions/624581/what-is-the-best-java-email-address-validation-method

		Pattern pattern = Pattern.compile("^.+@.+(\\.[^\\.]+)+$");
		Matcher matcher = pattern.matcher(s);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("error, please input a valid email for " + col.getColumnName());
		}
	}

	/**
	 * Check if column is only letters.
	 *
	 * @param s the s string which will be checked if it has only letters
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfColumnIsOnlyLetters(String s, Column col) {
		if (!s.matches("[a-zA-Z]+")) {
			throw new IllegalArgumentException("Error, you must only input letters for your " + col.getColumnName());
		}
	}

	/**
	 * Creates a checker of UserInput which will check if its inputed string matches the
	 * strings in list
	 * 
	 * @param list the Strings which are accepted values for the checker
	 */
	public static CheckerOfUserInput createCheckerForValues(String... list) {
		return (String s, Column col) -> {
			boolean found = false;
			for (int i = 0; i < list.length; i++) {
				if (s.equals(list[i])) {
					found = true;
				}
			}
			if (!found) {
				String error = "error, your " + col.getColumnName() + " value of " + s
						+ " did not match any valid value \r\n" + "please make sure to enter "
						+ generateEnterableValues(s, col, list) + " as its value";

				throw new IllegalArgumentException(error);
			}
		};
	}

	/**
	 * Generate error message for missing values
	 *
	 * @param s the user input
	 * @param col the column the user input belongs to
	 * @param list the list of acceptable values
	 * @return the error messsage for the values in list
	 */
	static String generateEnterableValues(String s, Column col, String[] list) {
		if (list.length == 0) {
			throw new IllegalArgumentException("error, the programmer entered an empty list to check values against");
		}
		String returned = list[0];
		for (int i = 1; i < list.length; i++) {
			returned += " or " + list[i];
		}
		if (!col.isNonNull()) {
			returned += " or nothing";
		}
		return returned;
	}

	/**
	 * Check if column is exactly max size.
	 *
	 * @param s the s string which will be checked if it is exaclt max size
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfColumnIsExactlyMaxSize(String s, Column col) {
		if (s.length() != col.getMaxSize()) {
			throw new IllegalArgumentException(
					"Error, your " + col.getColumnName() + " must be exaclty " + col.getMaxSize() + " characters long");
		}
	}

	/**
	 * Check if column is a valid phone number.
	 *
	 * @param s the s string which will be checked if it is a valid phone number
	 * @param col the col the column that the string belongs to
	 */
	public static void checkIfColumnIsValidPhoneNumber(String s, Column col) {
		Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}");
		Matcher matcher = pattern.matcher(s);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("error, make sure your " + col.getColumnName()
					+ " matches the following pattern \r\n " + "XXX-XXX-XXXX with the dashes");
		}
	}

	/**
	 * Check if column is valid date.
	 *
	 * @param s the s string which will be checked if it is a valid date
	 * @param col the col the column that the string belongs to
	 */
	//based on https://stackoverflow.com/questions/33968333/how-to-check-if-a-string-is-date
	public static void checkIfColumnIsValidDate(String s, Column col) {

		String dateformat = "MM/dd/yyyy";
		SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(s);
		} catch (ParseException pe) {
			throw new IllegalArgumentException(
					"error, make sure your " + col.getColumnName() + " matches the following pattern \r\n " + dateformat
							+ " with the slashes, also make sure your month is put first");

		}
	}

}
