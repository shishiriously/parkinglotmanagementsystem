package client;

import client.communicationToServer.ClientsCommunicationSystem;
import client.communicationToServer.ServerCommunicator;
import javax.swing.*;
import client.systems.specific.loginSystem.LoginSystem;
import java.awt.Dimension;

/**
 * ok this version is literally 1.1.  the only diffrence is I copy pasted the code in from 4.0, and changed a couple of
 * instantiations in main.
 */
public class PretendMain {

	public static void main(String[] args) {
		//makes a jframe to hold the stuff
		JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setTitle("application");
		
		
		ClientsCommunicationSystem system=new ClientsCommunicationSystem();
		ServerCommunicator communicator=system.getComunicator();
		
		LoginSystem log=new LoginSystem(communicator, "login");
		log.open();

		//adds the control systems default veiw to the frame.  the default veiw is the first thing you see when you
		//open the system.

		frame.getContentPane().add(log.getDefaultPanel());
		
		//AdminSystem testing = new AdminSystem(communicator, "Admin");
		//testing.open();
		//frame.getContentPane().add(testing.getDefaultPanel());
		frame.setSize(new Dimension(1920,1080));
		
		frame.setVisible(true);
		
	}

}
