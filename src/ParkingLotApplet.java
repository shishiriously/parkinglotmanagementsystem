import java.applet.Applet;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import client.communicationToServer.ClientsCommunicationSystem;
import client.communicationToServer.ServerCommunicator;
import client.systems.specific.loginSystem.LoginSystem;



public class ParkingLotApplet  extends Applet{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2758015990722978766L;

	public void init(){
		
				
				
				ClientsCommunicationSystem system=new ClientsCommunicationSystem();
				ServerCommunicator communicator=system.getComunicator();
				
				LoginSystem log=new LoginSystem(communicator, "login");
				log.open();

				//adds the control systems default veiw to the frame.  the default veiw is the first thing you see when you
				//open the system.
				
				log.getDefaultPanel().setVisible(true);
				log.getDefaultPanel().setPreferredSize(new Dimension(1920,1080));

				this.add(log.getDefaultPanel());
				
				//AdminSystem testing = new AdminSystem(communicator, "Admin");
				//testing.open();
				//frame.getContentPane().add(testing.getDefaultPanel());
				
		        this.setSize(new Dimension(1920,1080));
				this.setVisible(true);

    }

    public void paint(Graphics g){
    	super.paint(g);
    }
    public static void main(String[] args) {
    	
    	JFrame j=new JFrame();
    	ParkingLotApplet app=new ParkingLotApplet();
    	app.init();
    	j.add(app);
    	
    	j.setVisible(true);
    }
}
