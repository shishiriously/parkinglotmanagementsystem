package shared.stuffInTheServer;

import java.sql.*;
import java.util.ArrayList;

import client.dataFieldStuff.DataFieldList;
import shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction;

/**
 * this is a class which executes database functions, basically you can put in a database function into
 * the executor, and it will execute the function.
 */
public class DataBaseFunctionExecutor {
	private Connection dbConnection;
	private PreparedStatement statement;
	private static String databaseName = "ParkinglotManagementDatabase";
	private static String connectionInfo = "jdbc:mysql://localhost:3306/"+databaseName+"?useSSL=false",  
			login          = "FinalProjectDataTable",
			password       = "ensf";
	/**
	 * creates the databaseFunctionExector.
	 */
	public DataBaseFunctionExecutor() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			dbConnection = DriverManager.getConnection(connectionInfo, login, password);				
			
			System.out.println("Connected to: " + connectionInfo);
		}
		catch (SQLException e) {
			System.out.println("Make sure you are using the correct SQL credentials.  " + this.getClass().getName());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("Make sure that you have the .jar file in the lib folder added to the classpath");
			e.printStackTrace();
		}
	}

	/**
	 * Creates the database. Only needs to be created once.
	 */
	public void createDB()
	{
		try {
			String update = "CREATE DATABASE IF NOT EXISTS " + databaseName;
			statement = dbConnection.prepareStatement(update);
			statement.executeUpdate();
			System.out.println("Created Database " + databaseName);
		} 
		catch( SQLException e){
			System.out.println("Error Creating Database");
			e.printStackTrace();
		}
	}
	/**
	 * executes a single database function
	 * @param function the database function which will be executed
	 * @return the output of the database function
	 * @throws ServerError when there is an expected error in the execution
	 */
	public ArrayList<DataFieldList> executeDataBaseServerFunction(DataBaseFunction function) throws ServerError{
		return function.executeDataBasefunction(dbConnection);
	}

	
}