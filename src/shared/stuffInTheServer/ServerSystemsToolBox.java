package shared.stuffInTheServer;

import shared.clientAndServerCommunicationSystems.SenderToSocket;
import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.MessageFromServerToClient;

/**
 * The Class ServerSystemsToolBox which is basically what any Serverfunction uses to execute its task
 */
public class ServerSystemsToolBox {
	
	/** The sender to client. */
	private SenderToSocket<MessageFromServerToClient> senderToClient;
	
	/** The data base Function executor. */
	private DataBaseFunctionExecutor dataBaseExecutor;

	/**
	 * Instantiates a new server systems tool box.
	 *
	 * @param senderToClient senderToSocket for sending messages to the client
	 */
	public ServerSystemsToolBox(SenderToSocket<MessageFromServerToClient> senderToClient) {
		super();
		this.senderToClient = senderToClient;
		dataBaseExecutor = new DataBaseFunctionExecutor();
	}

	/**
	 * Gets the sender to client.
	 *
	 * @return the sender to client
	 */
	public SenderToSocket<MessageFromServerToClient> getSenderToClient() {
		return senderToClient;
	}

	/**
	 * Gets the data base function executor.
	 *
	 * @return the data base function executor
	 */
	public DataBaseFunctionExecutor getDataBaseFunctionExecutor() {
		return dataBaseExecutor;
	}

}
