package shared.stuffInTheServer;

import shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ErrorData;

/**
 * The Class ServerError. which is basically an expected error which occurs on the serverside,
 * it contains the errordata which stores information about the error
 */
public class ServerError extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 165595179475154310L;
	
	/** The data. */
	private ErrorData data;

	/**
	 * Instantiates a new server error.
	 *
	 * @param message the message displayed to the user
	 * @param type the type of error that happened.
	 */
	public ServerError(String message, ErrorData.ErrorType type) {
		super(message);
		data = new ErrorData(message, type);

	}

	/**
	 * Instantiates a new server error.
	 *
	 * @param data the data
	 */
	public ServerError(ErrorData data) {
		this.data = data;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public ErrorData getData() {
		return data;
	}
}
