package shared.clientAndServerCommunicationSystems;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.function.Consumer;


/**
 * The Class ReceiverFromSocket, which is a thing which recieves inputs from the socket.
 * basically, the reciever runs on its own thread, and each time that it recieves data from the socket,
 * it will forward it to the consumerOfOutput, which is set in the setConsumerOfOutput method. 
 *
 * @param <T> the generic type
 */
public class ReceiverFromSocket<T> implements Runnable{
	
	/** The inputs it will read from. */
	private ObjectInputStream inputs;
	
	/** The error message displayer, which it will display error messages using. */
	private Consumer<String> errorMessageDisplayer;
	
	/** is the reciever closed. */
	private boolean closed;
	
	/** The consumerOfOutput, which will recieve the output from the server each time it is recieved. */
	private Consumer<T> consumerOfOutput;
	
	/**
	 * Instantiates a new receiver from socket.
	 *
	 * @param socket the socket which it will recieve from
	 * @param errorMessageDisplayer the error message displayer which it will display error messages whenever something goes wrong
	 */
	public ReceiverFromSocket(Socket socket, Consumer<String> errorMessageDisplayer) {
		try {
			inputs=new ObjectInputStream(socket.getInputStream());
			this.errorMessageDisplayer=errorMessageDisplayer;
			closed=false;
		} catch (IOException e) {
			e.printStackTrace();
			errorMessageDisplayer.accept("error, we were unable to contact the server while receiving");
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
		while(closed==false) {
			receive();
			}
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			this.errorMessageDisplayer.accept("error, the programers messed up and a class was not found in the socket receiver");
		}
	}
	
	/**
	 * Receive, which will
	 * @throws ClassNotFoundException when it recieves something that is a class that it cannot find
	 * @throws ClassCastException when it recieves something which is not the same type as T
	 */
	protected void receive() throws ClassNotFoundException,ClassCastException {
		try {
			@SuppressWarnings("unchecked")
			T received=(T)inputs.readObject();
			consumerOfOutput.accept(received);
		} catch (IOException e) {
			e.printStackTrace();
			this.errorMessageDisplayer.accept("error, We were unable to receive from the server. please try again later");
			this.close();
		}catch(NullPointerException e) {
			if(this.consumerOfOutput==null) {
			this.errorMessageDisplayer.accept("error, the programers forgot to set the user<T> of the ReceiverFromSocket, after you"
					+ " guys get the ReceiverFromSocketFromSocketCommunicationSystemConstructor, remember to set its User<T> to"
					+ "whatever you guys want to receive the the object from the output stream");
			}
			throw e;
		}
	}
	
	/**
	 * Sets where the recieved outputs go whenever they are recieved, ie whenever something is recieved, it goes to the consumer
	 * @param receiver the place the server outputs go
	 */
	public void setConsumerOfOutput(Consumer<T> receiver) {
		this.consumerOfOutput = receiver;
	}
	
	/**
	 * closes the reciever, so it will no longer be able to recieve inputs. 
	 */
	public void close() {
		try {
			closed=true;
			inputs.close();
		} catch (IOException e) {
			e.printStackTrace();
			this.errorMessageDisplayer.accept("error, we were unable to stop communication with the server");
		}
	}
	
}
