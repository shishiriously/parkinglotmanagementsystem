package shared.clientAndServerCommunicationSystems;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.function.Consumer;
/**
 * this is a class which sends stuff to the socket, ie it will send whatever object you want 
 * through the socket, as long as it is serializable
 * @author brere
 * @param <T> the type which will be sent through the socket
 */


public class SenderToSocket<T> {
	
	ObjectOutputStream out;
	/**
	 * instantiates a new senderToSocket
	 * @param sock the socket which it will send inputs through
	 * @param errorMessageDisplayer the error message displayer it will use to display error messages if instantiation fails 
	 */
	public SenderToSocket(Socket sock,Consumer<String> errorMessageDisplayer) {
		try {
			out=new ObjectOutputStream(sock.getOutputStream());
		} catch (IOException e) {
			errorMessageDisplayer.accept("error, unable to connect to server please try again later");
			e.printStackTrace();
		}
	}
	/**
	 * sends whatever is being sent.  if it fails, it will throw an exception
	 * @param sent the thing that is being sent
	 * @param errorMessageDisplayer the thing which will display the error message
	 * @return whether the thing was sent successfully
	 * @throws IOException   whenever the send fails
	 */
	public void send(T sent) throws IOException {
	
			out.writeObject(sent);
			out.flush();
			out.reset();		
	}
	/**
	 * closes the socket, so it cannot be used.
	 * 
	 * @param errorMessageDisplayer the thing which will display error messages if the close operation fails
	 */
	public void close(Consumer<String> errorMessageDisplayer) {
		try {
			out.close();
		} catch (IOException e) {
			errorMessageDisplayer.accept("error, we were unable to close communication with the server.");
			e.printStackTrace();
		}
	}
}
